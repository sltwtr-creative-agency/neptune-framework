const path = require("path");
const glob = require('glob');

const docsFiles = new Promise((resolve, reject) => {
  const docsPath = path.join(__dirname, "../../../components/**/docs/*.md");
  let config = { 
    theme: {
      globals: {},
      header: {},
      footer: {},
      collection: {},
      product: {},
      cart: {},
      checkout: {},
      content: {},
      apps: {},
    },
    scripts: {},
    index: {}
  };
  let sortOrder = [];

  console.log("Creating theme docs");

  glob(docsPath, function (err, matches) {
    if (err) {
      console.log(err);
    }

    function f(key, value) {
      var object = {};
      var result = (object = {});
      var arr = key.split(".");
      for (var i = 0; i < arr.length - 1; i++) {
        object = object[arr[i]] = {};
      }
      object[arr[arr.length - 1]] = value;
      return (config = result);
    }

    var flat = [];
    function keypath(obj, path = "") {
      Object.keys(obj).forEach((key) => {
        flat.push(`${path ? `${path}.` : ``}${key}`);
        keypath(obj[key], `${path ? `${path}.` : ``}${key}`);
      });
      return flat;
    }

    matches.map((match) => {
      let object = config;
      path
        .basename(match, ".md")
        .split(".")
        .forEach((part) => {
          if (!object[part]) object[part] = {};
          object = object[part];
        });
    });

    sortOrder = keypath(config).map((p) => p + ".md");

    matches.sort((a, b) => {
      return sortOrder.indexOf(path.basename(a)) - sortOrder.indexOf(path.basename(b));
    })

    console.log(matches)

    resolve(matches);
  })
});

module.exports = docsFiles;