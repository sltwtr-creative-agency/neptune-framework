const fs = require("fs");
const path = require("path");
const glob = require('glob');

class SymSyncPlugin {
  constructor(config) {
    this.configObjects = Array.isArray(config)? config : Array(config)
  }
  apply(compiler) {
    compiler.hooks.done.tap(
      "SymSync",
      (
        stats /* stats is passed as an argument when done hook is tapped.  */
      ) => {
        this.configObjects.forEach(config => {

          config.host = makeHostPath(config)

          glob(config.origin, function (err, matches) {
            if (err) console.log(err);

            matches.forEach(match => {
              const originPath = match
              let pathExtension = getFileName(originPath)
              let destinationPath
              let hostPath

              if (config.origin.includes('**'))
                pathExtension = originPath.replace(config.origin.replace(/(\*).+/g, ''), '')
              
              destinationPath = !!path.extname(config.destination).length ? config.destination : path.join(config.destination, pathExtension)

              if (destinationPath.includes('[parentDir]'))
                destinationPath = destinationPath.replace('[parentDir]', originPath.split(path.sep).reverse()[1])
              
              if (destinationPath.includes('[file]'))
                destinationPath = destinationPath.replace(/(\[file]).+/, path.basename(originPath))

              hostPath = (() => {
                const fileName = getFileName(config.host)
                if (!!fileName.length && !fileName.includes('*')) 
                  return config.host 
                if (config.host.includes('*'))
                  return originPath
                return path.join(config.host, pathExtension)
              })() 

              if (hostPath.includes('[parentDir]')) {
                hostPath = hostPath.replace('[parentDir]', originPath.split(path.sep).reverse()[1])
              }
              
              if (hostPath.includes('[file]')) {
                hostPath = hostPath.replace(/(\[file]).+/, path.basename(originPath))
              }

              if (config.debug == true) {
                console.log('\n\n\n')
                console.log('originPath: ', originPath)
                console.log('pathExtension: ', pathExtension)
                console.log('destinationPath: ', destinationPath)
                console.log('hostPath: ', hostPath)

                return false
              }

              makeDirectories([destinationPath, hostPath], originPath)

              if (fs.existsSync(hostPath) && fs.existsSync(destinationPath)) 
                return false

              if (hostPath !== originPath) {
                fs.rename(originPath, hostPath, (err) => {
                  if (err) throw err
                  const originDirectory = !!path.extname(originPath).length ? path.dirname(originPath) : originPath

                  console.log('File moved for: ' + hostPath)
                  fs.symlink(path.relative(originDirectory, hostPath), originPath, !!path.extname(hostPath).length ? 'file' : 'dir', console.error)
                })
              }

              if (!fs.existsSync(destinationPath) && hostPath !== destinationPath) {
                const destinationDirectory = !!path.extname(destinationPath).length ? path.dirname(destinationPath) : destinationPath

                fs.symlink(path.relative(destinationDirectory, hostPath), destinationPath, !!path.extname(hostPath).length ? 'file' : 'dir', console.error)
              }
            })

          })
        })
      }
    );
  }
  
}

function makeDirectories(pathsArray, originPath) {
  Array.from(new Set(pathsArray)).forEach((relPath) => {
    let directoryPath = !!path.extname(relPath).length ? relPath.replace(path.basename(relPath), '') : relPath

    if (path.basename(directoryPath) == path.basename(originPath)) 
      directoryPath = directoryPath.replace(path.basename(originPath), '')

    if (fs.existsSync(directoryPath)) 
      return false

    fs.mkdirSync(directoryPath, { recursive: true })
  })
}

function makeHostPath(config) {
  if (!config.hasOwnProperty('host') || config.host == 'origin') 
    config.host = config.origin
  if (config.host == 'destination') 
    config.host = config.destination
  return config.host
}

function getFileName(filePath) {
  if (!!path.extname(filePath).length) return path.basename(filePath)
  return ''
}

module.exports = SymSyncPlugin;
