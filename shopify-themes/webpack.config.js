const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const glob = require("glob");
const tailwindcss = require("tailwindcss");
const purgecss = require("@fullhuman/postcss-purgecss");
const SymSyncPlugin = require("./build-tools/symsync");

const projectDir = path.join(__dirname, '..', '..')

module.exports = async (env, argv) => ({
  entry: glob.sync(path.join(projectDir, "components", "**", "index.js")).reduce(
    (acc, path) => {
      const entry_split = path.split("/").reverse();
      let entry = entry_split[1];

      acc[entry] = path;
      return acc;
    },
    { main: path.join(projectDir, "components", "main.js") }
  ),
  output: {
    path: path.join(projectDir, "theme"),
    filename: path.join(".", "assets", "[name].js"),
  },
  cache: true,
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
          },
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: [
                  tailwindcss,
                  argv.mode === "production"
                    ? require("cssnano")({ preset: "default" })
                    : null,
                ],
                minimize: true,
              },
            },
          },
          {
            loader: "sass-loader",
            options: {
              implementation: require("sass"),
            },
          },
        ],
      },
    ],
  },
  optimization: {
    runtimeChunk: "single",
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all",
        },
      },
    },
  },
  plugins: [
    new SymSyncPlugin({
      //debug: true,
      origin: path.join("components", "**", "*.liquid"),
      destination: path.join("theme", "[parentDir]", "[file]"),
      host: 'destination'
    }),
    new MiniCssExtractPlugin({
      filename: path.join(".", "assets", "[name].css"),
    }),
    // new BundleAnalyzerPlugin(),
  ],
});
