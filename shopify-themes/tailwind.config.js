const plugin = require("tailwindcss/plugin");

console.log('MODE!', process.env.mode)

const scales = {
  majorSecond: {
    '3xs': '0.624rem',
    '2xs': '0.702rem',
    'xs': '0.79rem',
    'sm': '0.889rem',
    'base': '1rem',
    'md': '1rem',
    'lg': '1.125rem',
    'xl': '1.266rem',
    '2xl': '1.424rem',
    '3xl': '1.602rem',
    '4xl': '1.802rem',
    '5xl': '2.027rem',
    '6xl': '2.281rem',
    '7xl': '2.566rem',
    '8xl': '2.887rem',
    '9xl': '3.247rem'
  },
  minorThird: {
    '3xs': '0.482rem',
    '2xs': '0.578rem',
    'xs': '0.694rem',
    'sm': '0.833rem',
    'base': '1rem',
    'md': '1rem',
    'lg': '1.2rem',
    'xl': '1.44rem',
    '2xl': '1.728rem',
    '3xl': '2.074rem',
    '4xl': '2.488rem',
    '5xl': '2.986rem',
    '6xl': '3.583rem',
    '7xl': '4.3rem',
    '8xl': '5.16rem',
    '9xl': '6.192rem'
  },
  perfectFourth: {
    '2xs': '0.563rem',
    'xs': '0.75rem',
    'sm': '0.88rem',
    'base': '1rem',
    'md': '1rem',
    'lg': '1.333rem',
    'xl': '1.777rem',
    '2xl': '2.369rem',
    '3xl': '3.157rem',
    '4xl': '4.209rem',
    '5xl': '5.61rem',
    '6xl': '7.478rem',
    '7xl': '9.969rem'
  },
  perfectFifth: {
    'xs': '0.444rem',
    'sm': '0.667rem',
    'base': '1rem',
    'md': '1rem',
    'lg': '1.5rem',
    'xl': '2.25rem',
    '2xl': '3.375rem',
    '3xl': '5.063rem',
    '4xl': '7.594rem',
    '5xl': '11.391rem',
    '6xl': '17.086rem',
    '7xl': '25.629rem'
  },
  augmentedFourth: {
    'xs': '0.5rem',
    'sm': '0.707rem',
    'base': '1rem',
    'md': '1rem',
    'lg': '1.414rem',
    'xl': '1.999rem',
    '2xl': '2.827rem',
    '3xl': '3.998rem',
    '4xl': '5.653rem',
    '5xl': '7.993rem',
    '6xl': '11.302rem',
    '7xl': '15.981rem'
  },
  goldenRatio: {
    '2xs': '0.236rem',
    'xs': '0.382rem',
    'sm': '0.618rem',
    'base': '1rem',
    'md': '1.618rem',
    'lg': '2.618rem',
    'xl': '4.236rem',
    '2xl': '6.854rem',
    '3xl': '11.089rem',
    '4xl': '17.942rem',
    '5xl': '29.03rem',
    '6xl': '46.971rem',
    '7xl': '75.999rem'
  },
  exponential: {
    '2xs': '0.125rem',
    'xs': '0.25rem',
    'sm': '0.382rem',
    'base': '1rem',
    'md': '1rem',
    'lg': '1.618rem',
    'xl': '2.618rem',
    '2xl': '4.236rem',
    '3xl': '6.854rem',
    '4xl': '11.090rem',
    '5xl': '17.944rem',
    '6xl': '29.034rem',
    '7xl': '46.978rem'
  }
};


module.exports = {
  scales:scales,
  // mode: 'jit',
  content: ['./theme/**/*.liquid'],
  theme: {
    extend: {
      minHeight: {
       '0': '0',
       '1/4': '25%',
       '1/2': '50%',
       '3/4': '75%',
       'full': '100%',
      },
      maxHeight: { "initial": "initial"},
      colors: {
        transparent: "transparent",
        current: "currentColor",
        primary: "var(--color-primary)",
        secondary: "var(--color-secondary)",
        tertiary: "var(--color-tertiary)",
        light: "var(--color-light)",
        dark: "var(--color-dark)",
        pop: "var(--color-pop)",
        highlight: "var(--color-highlight)",
        body: "var(--color-body)",
        header: "var(--color-header)",
      },
      fontFamily: {
        body: "var(--font-body-family)",
        heading: "var(--font-heading-family)",
        subheading: "var(--font-subheading-family)",
      },
      fontScales: scales,
      fontSize: scales.perfectFourth,
      height: {
        "10v": "10vh",
        "20v": "calc(20vh - var(--header-height-half))",
        "25v": "calc(25vh - var(--header-height-half))",
        "30v": "30vh",
        "40v": "40vh",
        "50v": "calc(50vh - var(--header-height-half))",
        "60v": "60vh",
        "70v": "calc(70vh - var(--header-height-half))",
        "75v": "calc(75vh - var(--header-height-half))",
        "80v": "80vh",
        "90v": "90vh",
        "100v": "100vh",
        "main": "calc(100vh - var(--header-height))",
        "fill": "-webkit-fill-available"
      },
      width: {
        "20v": "20vw",
        "content": "fit-content",
      },
      maxHeight: { 
        "initial": "initial",
        "main": "calc(100vh - var(--header-height))"
      },
      minHeight: { 
        "main": "calc(100vh - var(--header-height))",
        "75v": "calc(75vh - var(--header-height-half))"
      },
      maxWidth: { 
        "max-w-screen-3xl": "130rem"
      },
      flex: {
        full: '0 1 auto',
      },
      spacing: Object.assign(scales.perfectFourth,{
        off: '-10000%',
      }),
      transitionTimingFunction: {
        cubic: "cubic-bezier(0.215, 0.61, 0.355, 1)",
        "ease-cubic": "cubic-bezier(0.215, 0.61, 0.355, 1)",
      },
      transitionDuration: {
        zoom: '12000ms',
      }
    },
  },
 plugins: [
    require("@tailwindcss/aspect-ratio"),
    require("@tailwindcss/line-clamp"),
    // Neptune utilitiy variants
    plugin(function ({ addVariant, e }) {
      addVariant("active", "&.active")
      addVariant("check-active", "input:checked + &")
      addVariant("off-top", 'html[scroll-off-top="true"] &')
      addVariant("group-active", ":merge(.group).active &")
      addVariant("children", "& > *")
    }),
    plugin(function({ addUtilities }) {
      const newUtilities = {
        '.no-details-marker summary::-webkit-details-marker': {
          display: 'none',
        },
      };
      addUtilities(newUtilities, ['responsive']);
    })
  ],
  safelist:process.env.mode === 'development' ? [
    // {
    //   pattern://,
    //   variants: ['lg', 'hover', 'focus', 'active', 'group-active']
    // }
  ] : []
};
