const { execSync } = require('child_process');

try {
  const args = process.argv.slice(2);
  const includeArg = args.find(arg => arg.startsWith('--include='));
  const includeValue = includeArg ? includeArg.split('=')[1] : null;

  const BRANCH = execSync('git branch --show-current', { encoding: 'utf-8' }).trim();
  console.log(`Current branch: ${BRANCH}`);

  if (!BRANCH.includes('epic/') && (!includeValue || (includeValue && !BRANCH.includes(includeValue)))) {
    console.log("Branch name does not contain 'epic/'. Exiting.");
    process.exit(0);
  }

  const THEME_NAME = `${BRANCH} | @SLTWTR`;

  const STAGING_ID = execSync('shopify theme list --name="Staging" | grep -o \'#[0-9]\\+\' | sed \'s/#//\'', { encoding: 'utf-8' }).trim();
  console.log(`Staging ID: ${STAGING_ID}`);

  console.log(`Theme Name: ${THEME_NAME}`);

  let OUTPUT;
  try {
    OUTPUT = execSync(`shopify theme list --name="${THEME_NAME}"`, { encoding: 'utf-8' });
    console.log(`shopify theme list output: ${OUTPUT}`);
  } catch (error) {
    OUTPUT = error.stdout || error.stderr;
    console.log(`shopify theme list error output: ${OUTPUT}`);
  }

  console.log('ERROR TEST', /error/i.test(OUTPUT), OUTPUT)

  if (/error/i.test(OUTPUT)) {
    console.log('STAGING_ID', STAGING_ID)
    const pullOutput1 = !!STAGING_ID && execSync(`shopify theme pull --only="sections/*.json" --only="templates/*.json" --only="templates/**/*.json" --only="config/settings_data.json" --only="templates/*.json" --path="./theme/" -t="${STAGING_ID}"`, { encoding: 'utf-8' });
    console.log(`shopify theme pull output: ${pullOutput1}`);
    
    const NEW_THEME_ID = execSync('shopify theme share --path="./theme/" | grep -o \'#[0-9]\\+\' | sed \'s/#//\'', { encoding: 'utf-8' }).trim();
    // console.log(`New Theme ID: ${NEW_THEME_ID}`);

    const renameOutput = execSync(`shopify theme rename -t="${NEW_THEME_ID}" -n="${THEME_NAME}"`, { encoding: 'utf-8' });
    console.log(`shopify theme rename output: ${renameOutput}`);
  } else {
    const NEW_THEME_ID = execSync(`shopify theme list --name="${THEME_NAME}" | grep -o \'#[0-9]\\+\' | sed \'s/#//\'`, { encoding: 'utf-8' }).trim();
    // console.log(`New Theme ID: ${NEW_THEME_ID}`);

    const pushOutput = execSync(`shopify theme push -t="${NEW_THEME_ID}" --path="./theme/" --ignore="sections/*.json" --ignore="templates/*.json" --ignore="templates/**/*.json" --ignore="config/settings_data.json"`, { encoding: 'utf-8' });
    console.log(`shopify theme push output: ${pushOutput}`);
  }
} catch (error) {
  console.error(`Error: ${error.message}`);
  process.exit(1);
}
