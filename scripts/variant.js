import _n from './_n'

/**
 * variant selection
 * @SLTWTR
 */
var variant = {

  id: false,
  available: false,
  selecting_el: {},
  form: {},

  init: () => {

    if( _n.qsa('#selected_or_first_available_variant').length )
      variant.set( _n.qs('#selected_or_first_available_variant').innerHTML.trim() )

  },

  select: (el, callback) => {

    variant.selecting_el = el
    
    variant.form = _n.parents(el, 'form')

    var options = []

    if(_n.qsa('[data-variants]:checked').length) {
    
      _n.qsa('[data-variants]:checked', variant.form).forEach( ( e ) => {

        options.push(
          e.getAttribute('data-variants').split(',').filter( (v)=> {
            return !!v
          })
        )
      })

    } else if (_n.qsa('[data-variants].active').length) {

      _n.qsa('[data-variants].active', variant.form).forEach( ( e ) => {

        options.push(
          e.getAttribute('data-variants').split(',').filter( (v)=> {
            return !!v
          })
        )
      })

    }

    // console.log('options',options)
    
    variant.id = Number(options.reduce((a, b) => a.filter(c => b.includes(c)))[0])

    variant.available = variant.availability()

    if(!!variant.selecting_el.getAttribute('data-variants'))
      variant.family()

    if(!!callback) callback( variant.id );

    // console.log( variant.id, variant.available)

    variant.set(variant.id)

    return variant.id

  },

  availability: () => {

    var options = []

    _n.qsa('[data-variants]:checked', variant.form).forEach( ( e ) => {

      options.push(
        e.getAttribute('data-available').split(',').filter( ( v ) => {
          return !!v
        })
      )

    })

    var available_id = Number(options.reduce(function(a, b){
      return a.filter(function(c){
        return b.includes(c)
      })
    })[0])

    return !isNaN(available_id)      

  },

  family: () => {

    _n.qsa('[data-variants]', variant.form).forEach( ( e ) => {

      if(e.name==variant.selecting_el.name) return false;

      var options = [variant.selecting_el.getAttribute('data-variants').split(',').filter(function(v){return !!v})]
      
      options.push(e.getAttribute('data-variants').split(',').filter(function(v){return !!v}))

      var shared = options.reduce(function(a,b){
        return a.filter(function(c){
          return b.includes(c)
        })
      })

      var shares = shared.length > 0
      
      if(shares){
        e.removeAttribute('data-disabled')
      } else {
        e.setAttribute('data-disabled','true')
      }
    
    })

  },

  set: (id) => {

    _n.qsa('form [data-variants]').forEach( ( el ) => {

      if(el.getAttribute('data-variants').includes(id)) {

        variant.form = _n.parents(el,'form')

        el.checked = true;
        el.selected = true; 

        if(el.matches('option'))
          _n.parents(el,'select').value = el.value

        // set the no-js basic
        if(_n.qsa('[name=id]',variant.form).length)
          _n.qs('[name=id]',variant.form).value = id
      }

    })

    window.history.replaceState( {} , document.title, document.location.href.split('?')[0] + '?variant=' + id );

    _n.trigger(document, 'variant:set', false ,true, {variant:id,form:variant.form})

  }

};

export default variant;