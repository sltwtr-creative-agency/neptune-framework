import Neptune from'./neptune';
import _n from './_n';

Neptune.transport = {
  init: function( p ){

    Neptune.debug.info('transport:init', {el:p||document})

    _n.qsa('[neptune-transport]',p || document).forEach( el => {

    let _transport =[{}]

      const attributeConfig = el.getAttribute('neptune-transport')
            .replace(/'/g,'"')
            .replace(/`/g,"'")
            .replace(/</g,'[')
            .replace(/>/g,']')

      try {
        _transport = 
          USON.parse(attributeConfig, 'array')[0]
      }
        catch(error) {
          if (window.Shopify && window.Shopify.theme.role == 'development') console.log('USON parse error on', attributeConfig)
          try {
            _transport = JSON.parse(attributeConfig)
          } catch(error) {
            if (window.Shopify && window.Shopify.theme.role == 'development') console.error(attributeConfig,error)
          }
      }

      
      if(!!_transport.breakpoints){

        var onbreak = _n.debounce(function() {
          Neptune.transport.action(el, _transport)
        }, 250);

        window.addEventListener('resize', onbreak);

      }

      Neptune.transport.action(el, _transport)

    });

  },
  action: (el, _transport) => {

    // if(el.parentNode.classList.contains('transport-recipient')) return false;

    _transport = _n.copy(_transport)

    // handle breakpoints
    if(!!_transport.breakpoints){
      var vbps = Object.keys(_transport.breakpoints).filter(b=>b<window.innerWidth).reverse()
      if (vbps.length) {
        for(let key in _transport.breakpoints[vbps[0]]){
          _transport[key] = _transport.breakpoints[vbps[0]][key]
        }
      }
    }

    if(_n.exists(_transport.before)){  
      // if(_n.qs(_transport.before).parentNode.contains(el)) return false;
      _n.qs(_transport.before).before(el)
      _n.qs(_transport.before).parentNode.classList.add('transport-recipient')
    }

    if(_n.exists(_transport.after)){
      // if(_n.qs(_transport.after).parentNode.contains(el)) return false;
      _n.qs(_transport.after).after(el)
      _n.qs(_transport.after).parentNode.classList.add('transport-recipient')
    }

    if(_n.exists(_transport.append)){
      if(_n.qs(_transport.append).contains(el)) return false;
      try {
        _n.qs(_transport.append).append(el)
        _n.qs(_transport.append).classList.add('transport-recipient')
      } catch(err) {}
    }

    if(_n.exists(_transport.prepend)){
      if(_n.qs(_transport.prepend).contains(el)) return false;
      _n.qs(_transport.prepend).prepend(el)
      _n.qs(_transport.prepend).classList.add('transport-recipient')
    }

    if(_n.exists(_transport.replace)){
      _n.qs(_transport.replace).innerHTML = ''
      _n.qs(_transport.replace).append(el)
      _n.qs(_transport.replace).classList.add('transport-recipient')
    }

  }
}

// document.addEventListener('periscope:preview', e => {
//   _n.qsa('.transport-recipient [neptune-transport]',e.detail.info.el).forEach( el => {
//     document.body.append(el)
//     console.log('transported out', el)
//   })
// })
// _n.trigger(document,'periscope:preview',false,true,{el:el,url:url,meta:meta})

window.Neptune = Neptune

export default Neptune.debug;
