import Neptune from './neptune'
import _n from './_n'
import USON from '../../node_modules/uson'

Neptune.stratosphere = {

  init: p => {

  	window.dataLayer = window.dataLayer || []

    p = p || document

    _n.qsa('[neptune-stratosphere]:not([neptune-stratosphere-processed])',p).forEach( (el, i) => {

      try {
    
        el.stratosphere = USON.parse(el.getAttribute('neptune-stratosphere'))[0]

        if(el.stratosphere.data.position=='auto')
          el.stratosphere.data.position = i+1;

        if(!!el.stratosphere.awaitChildren) return false;

  	  	setTimeout( () => {
  	  		Neptune.stratosphere.process(el)
  	  	}, el.stratosphere.delay || 0)

      } catch(err) { 
        console.log(err)
      }
      
    })
  
  },
  process: el => {

  	if(!el.stratosphere ) return false

  	if(!!el.stratosphere.events){
	  	el.stratosphere.events.forEach( evt => {

	  		if(evt.on == 'load' || !evt.on){
	  			window.dataLayer.push(JSON.parse(JSON.stringify(evt.push).replace(`"_this"`,JSON.stringify(el.stratosphere.data))))
	  		} else {
					el.addEventListener(evt.on, e=>{
						window.dataLayer.push(JSON.parse(JSON.stringify(evt.push).replace(`"_this"`,JSON.stringify(el.stratosphere.data))))
					})
	  		}

	  	})
	  }

    // ascend to parent
  	if(!!el.stratosphere.parent && !!el.stratosphere.data){

      if(!_n.parents(el, '[neptune-stratosphere]', true)) return false;
      try {
    	   el.stratosphere.parent.split('.').reduce((o,i)=>o[i], _n.parents(el, '[neptune-stratosphere]', true).stratosphere.data).push(el.stratosphere.data)
  	  } catch(err) {}
  		// if it's the last child of a parent that is awaiting children, process the parent
  		if(_n.array(_n.qsa('[neptune-stratosphere]:not([neptune-stratosphere-processed])',_n.parents(el, '[neptune-stratosphere*=awaitChildren]', true))).reverse()[0] == el){
  			Neptune.stratosphere.process(_n.parents(el, '[neptune-stratosphere*=awaitChildren]', true))
  		}

    }

    el.setAttribute('neptune-stratosphere-processed', true)

  }

}
window.Neptune = Neptune

export default Neptune.stratosphere
