const Intersection = {
	init: p => {
		p = p || document

		_n.qsa('[neptune-intersection]', p).forEach(element => {
			let defaultOptions = { threshold: 1 }
			let config = USON.parse(element.getAttribute('neptune-intersection'))[0]
			let options = config.options || defaultOptions
			let newObserver = true
			let observer
			
			Intersection.oberservers.forEach((obs, ind, arr) => {
				if (JSON.stringify(obs.options) == JSON.stringify(options)) {

					newObserver = false
					observer = obs.observer

					arr.length = ind + 1
				}
			})

			if (newObserver) {
				observer = new IntersectionObserver(Intersection.handleEntries, options)
				Intersection.oberservers.push({ options: options, observer: observer })
			}
		
			observer.observe(element)
		});
	},
	oberservers: [],
	handleEntries: (entries) => {

		entries.forEach(entry => {
			const engagements = USON.parse(entry.target.getAttribute('neptune-intersection'))[0]

			if (entry.isIntersecting) {
				if (engagements.in.targets) engagements.in.targets.forEach(action => Intersection.handleEngageActions(action, entry.target))
			} else {
				if (engagements.out.targets) engagements.out.targets.forEach(action => Intersection.handleEngageActions(action, entry.target))
			}
		})

	},
	handleEngageActions: (action, target) => {
		let selector = _n.qs(action.selector)

		if (action.selector.includes('_self')) {
			if (action.selector == '_self') {
				selector =  target
			} else {
				selector = _n.qs(action.selector.replace('_self',''), target)
			}
		}
		if (action.selector.includes('_parent')) {
			if (action.selector == '_parent') {
				selector = target.parentNode
			} else {
				selector = _n.qs(action.selector.replace('_parent',''), target.parentNode)
			}
		} 
		Neptune.engage.action(selector, action)
	}
}

Neptune.intersection = Intersection

export default Intersection
