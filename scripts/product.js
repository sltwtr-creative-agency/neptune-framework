// Handles all things product related,
// Requires Neptune.cart to add to cart in Shopify

Neptune.product = {

  init: p => {

    p = p || document

    Neptune.products = Neptune.products || []

    _n.qsa('[neptune-product]',p).forEach( el => {

      _n.trigger(document,'product:init',false,true,{el:el})

      el.instance = _n.random(15)

      if(_n.exists('[neptune-product-data]', el)) {
        el.product = JSON.parse(_n.qs('[neptune-product-data]', el).innerHTML)
      } else {  
        el.product = JSON.parse(el.getAttribute('neptune-product'))
      }

      // find missing variants from possible options array and create them to fill in all options
      // _n.combine(el.product.options_with_values.map(o=>o.values)).map(o=>JSON.stringify(o)).filter(x => !el.product.variants.map(v=>{
      //   return JSON.stringify(v.options.reverse())
      // }).includes(x)).map(o=>{
      //   o = JSON.parse(o)
      //   console.log(o)
      //   el.product.variants.push(
      //     {
      //       id:0,
      //       price:el.product.variants[0].price,
      //       options:o,
      //       option1:o[0],
      //       option2:o[1] || null,
      //       option3:o[2] || null,
      //       inventory_quantity:0,
      //       available:false,
      //       bogus:true
      //     }
      //   )
      // });

      if(!el.variant) {

        if(_n.urlparams.get('variant') && el.getAttribute('data-enable-history-state'))
          el.variant = el.product.variants.filter((v)=>{return v.id==_n.urlparams.get('variant')})[0]
        
        if(!el.variant && el.product.variants)
          el.variant = el.product.variants.filter((v)=>{return v.available})[0]

        if(!el.variant && el.product.variants)
          el.variant = el.product.variants[0]

      }

      el.variant.quantity = 1
      Neptune.product.variant_in_cart(el)

      _n.trigger(el,'product:init',false,true,el.variant)
      _n.trigger(document,'product:init',false,true,{el:el,variant:el.variant})
      
      Neptune.debug.info('product:init', {el:el,variant:el.variant})

      if(!!el.product.image_constraint_options){
        if(el.product.options_with_values.map(o=>o.name).filter(v => el.product.image_constraint_options.includes(v)).length){
          Neptune.product.constrain_images(el)
        }
      }

      el.properties = {}
      
      el.components = {}

      Neptune.products.push({el:el,product:el.product,variant:el.variant})
      
      Neptune.product.refresh()

    })
  
  },

  refresh: () => {
    Neptune.liquid.load('ProductOptions')
  },

  variant_in_cart: el => {

    return el.variant.quantity_in_cart = _n.sum(cart.items.filter(i=>i.id===el.variant.id).map(i=>i.quantity))

  },

  option: input => {

    if(!!input.getAttribute('neptune-option')){
      var name = input.getAttribute('neptune-option')
    } else {
      var name = input.getAttribute('name').split('[')[1].split(']')[0]
    }

    const value = input.value
    const el = _n.parents(input,'[neptune-product]')

    if (el.product && el.product.options_with_values) {
      el.product.options_with_values.map(option => {
        if (name == option.name) {
          option.selected = value;
        }
      });
      
      _n.trigger(el, 'product:optionSelected', true, false, el.variant);
      Neptune.debug.info('product:optionSelected', { el: el, variant: el.variant });
  
      Neptune.product.variant.get(el, el.product.options_with_values.map(o => o.name).indexOf(name));
      Neptune.liquid.load('ProductOptions');
  
      if (el.product.image_constraint_options && el.product.image_constraint_options.includes(name))
        Neptune.product.constrain_images(el);
    }     
  },

  variant: {
    get: (el, oi) => {

      const options = _n.qsa('select[neptune-option], [neptune-option]:checked', el).map(input=>{return input.value.replace(/\"/g, '&quot;')})

      if (options.length == 0) {
        return false;
      } 
      
      el.variant = el.product.variants.filter( v => {
        function parseHtmlEntities(str) {
          return str.replace(/&#([0-9]{1,4});/gi, function(match, numStr) {
            var num = parseInt(numStr, 10); // read num as normal number
            return String.fromCharCode(num);
          });
        }
        const parsedOptions = v.options.map(o => parseHtmlEntities(o))
        return _n.equal(parsedOptions, options)
      })[0]
      
      if(!el.variant) el.variant = {
        id:0,
        options:options,
        option1:options[0] || null,
        option2:options[1] || null,
        option3:options[2] || null,
        price:el.product.variants[0].price,
        inventory_quantity:0,
        available:false,
        bogus:true
      };

      el.variant.quantity = 1
      Neptune.product.variant_in_cart(el)

      _n.trigger(el,'product:variantSelected',false,true,el.variant)
      _n.trigger(document,'product:variantSelected',false,true,{el:el,variant:el.variant})
      Neptune.debug.info('product:variantSelected', {el:el,variant:el.variant})
      
      // console.log('variant.get', el);

      if(el.getAttribute('data-enable-history-state')){
        window.history.replaceState(
          window.history.state,
          document.title,
          document.location.pathname+'?'+_n.urlparams.set('variant',el.variant.id)
        );
      }
      
      return el.variant
    },
    set: (el,id) => {

    }
  },

  constrain_images: el => {

    _n.trigger(el,'product:imageConstraint',false,true,el.variant)
    Neptune.debug.info('product:imageConstraint', {el:el,variant:el.variant})

    if(!!Neptune.liquid.topics.VariantImages)
      Neptune.liquid.load('VariantImages')

  },

  quantity: {
    up: input => {
      const el = _n.parents(input,'[neptune-product]')
      if(el.variant.inventory_quantity > el.variant.quantity)
        el.variant.quantity++
      Neptune.liquid.load('ProductOptions')
      _n.trigger(el,'product:quantityChanged',false,true,el.variant)
      Neptune.debug.info('product:quantityChanged', {el:el,variant:el.variant})
    },
    down: input => {
      const el = _n.parents(input,'[neptune-product]')
      if(el.variant.quantity > 1)
        el.variant.quantity--
      Neptune.liquid.load('ProductOptions')
      _n.trigger(el,'product:quantityChanged',false,true,el.variant)
      Neptune.debug.info('product:quantityChanged', {el:el,variant:el.variant})
    }
  },

  addToCart: el => {
    if(_n.parents(el,'[neptune-product]')) el = _n.parents(el,'[neptune-product]');

    _n.trigger(el,'product:addToCart',true,true,{
      product: el.product,
      variant: el.variant
    })

    let items = [el]
      .filter(el => !!el.product && el.variant)
      .map(el => ({
        id: parseInt(el.variant.id),
        quantity: el.variant.quantity,
        properties: {
          ...el.properties,
          ...(el.product.tags ? { _tags: el.product.tags } : {}),
          _compare_at_price: el.variant.compare_at_price || el.variant.price
        }
      }));
  
    // Handle component items
    if(!!el.components && typeof el.components === 'object') {
      const componentItems = Object.entries(el.components).map(([componentName, component]) => {
        const componentProperties = component.properties || {};
        let componentTags = component.tags || []; // Use tags from the component object
        
        return {
          id: parseInt(component.id || component),
          quantity: component.quantity || 1,
          properties: {
            ...componentProperties,
            _bundle: 'component',
            _component: componentName,
            _tags: componentTags
          }
        };
      });
      items = items.concat(componentItems);
    }
  
    // Check if product is part of a bundle (either primary or component)
    const isBundleProduct = !!el.properties._bundle && ['primary', 'component'].includes(el.properties._bundle);
    const groups = cart.items.map(i => Number(i.properties._group || 0));
    const maxGroup = groups.length > 0 ? Math.max(...groups) : 0;
    const nextGroupNumber = maxGroup + 1;
  
    // Modify items for bundled products and ensure all properties are preserved
    items = items.map(item => ({
      ...item,
      properties: {
        ...item.properties,
        ...(isBundleProduct && !!Neptune.product.instanced ? {
          _instance: el.instance,
          _group: nextGroupNumber
        } : {})
      }
    }));
  
    if (!!Neptune.product.instanced) el.instance = _n.random(15);
  
    items = items.filter(item => !!item.id);
  
    // Log items for debugging
    // console.log('Items before adding to cart:', JSON.stringify(items, null, 2));
  
    return Neptune.cart.add(items);
  }
}  

window.Neptune = Neptune

export default Neptune.product
