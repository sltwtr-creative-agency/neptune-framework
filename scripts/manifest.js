import Neptune from './neptune'
import _n from './_n'

import USON from '../../node_modules/uson'


Neptune.manifest = {

  init: p => {

    p = p || document

    _n.qsa('[neptune-manifest]',p).forEach( el => {
    
      el.manifest = USON.parse(el.getAttribute('neptune-manifest'))[0]
      
	  	// if (!!el.manifest.observe)
	  	// 	Neptune.manifest.observe(el)

	  	setTimeout( () => {
	  		Neptune.manifest.process(el)
	  	}, el.manifest.delay || 0)
      
    })
  
  },

  process: el => {

  	if (!!el.manifest.map){
  		el.manifest.data = el.manifest.data || {}
  		for(let prop in el.manifest.map) {
  			const target = _n.qs(el.manifest.map[prop].source, el) || el 
  			el.manifest.data[prop] = Neptune.manifest.extract(target)
  		}
  	}

  	_n.array(el.manifest.then).forEach( _then => {
	  	if(!!_then['liquid:load'] && !!Neptune.liquid){
	        Neptune.liquid.load(_then['liquid:load'].topic,el.manifest.data)
	    }
  	})

  },

  extract: (element, isSVG) => {
		var getAttributes = function (attributes, format) {
			if(format=='object'){
				var atts = {}
				Array.from(attributes).forEach(attribute=> {
					atts[attribute.name] = attribute.value
				});
				return atts
			}
			return Array.prototype.map.call(attributes, function (attribute) {
				return {
					att: attribute.name,
					value: attribute.value
				};
			});
		};
		return Array.prototype.map.call(element.childNodes, (function (node) {
			var details = {
				content: node.childNodes && node.childNodes.length > 0 ? null : node.textContent,
				// atts: node.nodeType !== 1 ? [] : getAttributes(node.attributes, 'array'),
				atts: node.nodeType !== 1 ? [] : getAttributes(node.attributes, 'object'),
				type: node.nodeType === 3 ? 'text' : (node.nodeType === 8 ? 'comment' : node.tagName.toLowerCase()),
				data: node.dataset,
				node: node
			};
			details.isSVG = isSVG || details.type === 'svg';
			details.children = Neptune.manifest.extract(node, details.isSVG);
			return details;
		}));

  },

  observe: el => {

		// // Options for the observer (which mutations to observe)
		// const config = el.manifest.observe || { attributes: true, childList: true, subtree: true };

		// // Callback function to execute when mutations are observed
		// const callback = function(mutationsList, observer) {
	 //    // Use traditional 'for loops' for IE 11
	 //    for(let mutation of mutationsList) {
	 //        if (mutation.type === 'childList') {
	 //            console.log('A child node has been added or removed.');
	 //        }
	 //        else if (mutation.type === 'attributes') {
	 //            console.log('The ' + mutation.attributeName + ' attribute was modified.');
	 //        }
	 //        Neptune.manifest.process(el)
	 //    }
		// 	if(!!el.manifest.observe.once)
		// 		el.manifestObserver.disconnect();
		// };

		// // Create an observer instance linked to the callback function
		// el.manifestObserver = new MutationObserver(callback);

		// // Start observing the target node for configured mutations
		// el.manifestObserver.observe(el, el.manifest.observe);

		// if(!!el.manifest.observe.now)
		// 	el.manifestObserver.disconnect();

  }

}

window.Neptune = Neptune

export default Neptune.manifest