import Neptune from "./neptune";
import _n from "./_n";

// Dynamic aria attributes using mutation observers.

Neptune.aria = {
	current_arias: [],
	init: () => {
		Neptune.aria.attachHidden()
	},
	attachObserver: (el, action, config) => {
		const observer = new MutationObserver(function(mutations) {
			mutations.forEach(function(mutation) {
				action(mutation)
			});
		});

		if (el && action && config) {
			observer.observe(el, config)
			Neptune.aria.current_arias.push({el: el, observer: observer})
		} else {
			console.error("Observers need an element, action, and config")
		}
	},
	detachObservers: () => {
		const observers = Neptune.aria.current_arias
		if (observers.length) {
			observers.forEach((observer) => {observer.observer.disconnect()})
		} else {
			console.warn("There are currently no dynamic aria elements")
		}
	},
	attachHidden: () => {
		const elements = _n.qsa('[dynamic-aria-hidden]')

		elements.forEach(element => {
			const action = (mutation) => {

				if (mutation) {
					Neptune.aria.setAriaHidden(element)
				}
			};
			const config = {
				attributeFilter: ['style', 'class']	
			};

			Neptune.aria.setAriaHidden(element)
			Neptune.aria.attachObserver(element, action, config)
		})
	},
	setAriaHidden: (element) => {
		const styles = getComputedStyle(element)

		if (
			element.height == 0 ||
			element.width == 0 ||
			styles.display == 'none' ||
			styles.visibility == 'hidden' ||
			styles.opacity == '0'
		) {
			element.setAttribute('aria-hidden', 'true')
			Neptune.brig.setTabbable(element, false)
		} else {
			element.setAttribute('aria-hidden', 'false')
			Neptune.brig.setTabbable(element, true)
		}
	}
}

window.Neptune = Neptune;

export default Neptune.aria;