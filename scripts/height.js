


Neptune.height = {

  init: (p, matchSelector) => {

    p = p || document
    matchSelector = matchSelector || '[neptune-height]'

    _n.qsa(matchSelector,p).forEach( el => {
    
      _n.qsa('[neptune-height]').forEach(el=>{el.style.height=null});
      _n.qsa('[neptune-height]').forEach(el=>{
        el.style.height=`${(Math.max(..._n.qsa(matchSelector, p).filter(o=>Math.round(o.getBoundingClientRect().top) == Math.round(el.getBoundingClientRect().top)).map(o=>o.scrollHeight)))}px`;

      })
      
    })
  
  },

  process: el => {

      

  },

  match: (element, qs) => {
    

  },

  observe: el => {


  }

}

window.Neptune = Neptune

export default Neptune.manifest