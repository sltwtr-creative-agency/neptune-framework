Neptune.draggable = {

  init: p => {

    p = p || document;
    
    _n.qsa('[neptune-draggable]', p).forEach( el => {

      el.parentNode.moveable_children = _n.qsa('[neptune-draggable]', el.parentNode).map(mc=>{
        mc.initial_transform = window.getComputedStyle(mc).transform;
        if(mc.hasAttribute('neptune-draggable-reverse')) mc.reverse = true;
        return mc;
      });

      el.parentNode.addEventListener("touchstart", Neptune.draggable.dragStart, false);
      el.parentNode.addEventListener("touchend", Neptune.draggable.dragEnd, false);
      el.parentNode.addEventListener("touchmove", Neptune.draggable.drag, false);

      el.parentNode.addEventListener("mousedown", Neptune.draggable.dragStart, false);
      el.parentNode.addEventListener("mouseup", Neptune.draggable.dragEnd, false);
      el.parentNode.addEventListener("mousemove", Neptune.draggable.drag, false);
      
    })

  },

  dragStart: e => {

    const el = e.target

    el.xOffset = el.xOffset || 0;
    el.yOffset = el.yOffset || 0;

    if (e.type === "touchstart") {
      el.initialX = e.touches[0].clientX - el.xOffset;
      el.initialY = e.touches[0].clientY - el.yOffset;
    } else {
      el.initialX = e.clientX - el.xOffset;
      el.initialY = e.clientY - el.yOffset;
    }

    // if (e.target === dragItem) {
      el.dragging = true;
    // }

  },

  dragEnd: e => {
    const el = e.target;
    el.initialX = el.currentX;
    el.initialY = el.currentY;

    el.dragging = false;
  },

  drag: e => {
    
    const el = e.target;
    
    if (el.dragging) {
    
      e.preventDefault();
    
      if (e.type === "touchmove") {
        el.currentX = e.touches[0].clientX - el.initialX;
        el.currentY = e.touches[0].clientY - el.initialY;
      } else {
        el.currentX = e.clientX - el.initialX;
        el.currentY = e.clientY - el.initialY;
      }

      el.xOffset = el.currentX;
      el.yOffset = el.currentY;


      el.moveable_children.forEach( mc => {
        if(mc.reverse){
          mc.style.transform = `${mc.initial_transform} translate3d(${el.currentX * -1}px, ${el.currentY * -1}px, 0)`;
        } else {
          mc.style.transform = `${mc.initial_transform} translate3d(${el.currentX}px, ${el.currentY}px, 0)`;
        }
      })
      
      // console.log(e.target.currentX, e.target.currentY, e.target);
    }
  }

}

window.Neptune = Neptune

export default Neptune.complete