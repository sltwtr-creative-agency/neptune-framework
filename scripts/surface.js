import Neptune from'./neptune';
import _n from './_n';
import USON from '../../node_modules/uson'

import engage from './engage';
Neptune.engage = Neptune.engage || engage

/**
 * surface
 * @SLTWTR
 */
Neptune.surface = {
  direction:'up',
  position: {},
  scrollPos: 0,
  scrolled: false,
  bottomPos: document.body ? (document.body.getBoundingClientRect()).bottom : false,
  init: function(p) {
    if (!Neptune.surface.bottomPos) Neptune.surface.bottomPos = (document.body.getBoundingClientRect())
    Neptune.surface.listener(p)
    Neptune.surface.handler(p,false)
  },
  listener: function(p){
    p = p || window
    // p.addEventListener("scroll",_n.debounce(e=>{
    //   Neptune.surface.scrolled = true; Neptune.surface.handler( p )
    // }));
    p.addEventListener("scroll",_n.throttle(e=>{
      Neptune.surface.scrolled = true; Neptune.surface.handler( p )
    }));
  },
  handler: ( p) => {

    p = p || document
    if(p==window) p = document
    
    let b = p 

    if(p==document) b = document.body
    if(!b) return false;

    let direction = 'down'
    if (b.getBoundingClientRect().top > Neptune.surface.scrollPos && Neptune.surface.scrolled)
      direction = 'up';

    if(Neptune.surface.direction != direction){
      Neptune.surface.direction = direction
      _n.trigger(document, "surface:changeDirection", false, true, {});
    }
    _n.trigger(document, "surface:scrolled", false, true, {});

      
    Neptune.surface.scrollPos = (b.getBoundingClientRect()).top
    Neptune.surface.position = (b.getBoundingClientRect())
    Neptune.surface.emerge(b)
    Neptune.surface.inview(b)

  },
  inview: p => {


  },
  emerge: p => {

    p == p || window

    _n.qsa('[neptune-surface-state]', p).forEach(el => {
      
      el.rect = el.getBoundingClientRect();
      
      el.scrollState = {
        from_top:Math.round(el.rect.top),
        from_bottom:Math.round((window.innerHeight || document.documentElement.clientHeight) - el.rect.bottom)
      }
      el.scrollState.from_center = Math.round(el.rect.top - window.innerHeight/2 + el.clientHeight/2)+'px';
      
      el.scrollState.total = (el.scrollState.from_top > 0 && el.scrollState.from_bottom > 0)
      
      el.style.setProperty("--from-top", el.scrollState.from_top);
      el.style.setProperty("--from-bottom", el.scrollState.from_bottom);
      el.style.setProperty("--from-center", el.scrollState.from_center);
      
      
      el.setAttribute('neptune-surface-state',JSON.stringify(el.scrollState).replace(/\"/g,''))
    })


    _n.qsa('[neptune-surface]', p).forEach( (el) => {

      let _surface = [{}]

      if(!!el.getAttribute('neptune-surface')){
        try {
          _surface = _n.array(
            USON.parse(el.getAttribute('neptune-surface'))
          )
        }
        catch(error) {
          Neptune.debug.error(error,el,'Surface')
        }
      }

      _surface.forEach( (o)=> {
        
        let trigger = el
        let offset = 0

        // console.log('surface',o)

        o.direction = o.direction || 'down'

        if(!!o.trigger && _n.exists(o.trigger)) trigger = _n.qs(o.trigger)

        if(!!o.offset){
          if(!isNaN(o.offset)){
            offset = Number(o.offset) 
          } else {
            if(o.offset == '_self') offset = _n.height(el) //+ el.getBoundingClientRect()['top']
            if(o.offset == '-_self') offset = -1 * (_n.height(el) ) //+ el.getBoundingClientRect()['top'])
            if(_n.exists(o.offset)) {
              offset = _n.height(_n.qs(o.offset))
              if(window.getComputedStyle(_n.qs(o.offset)).position == 'fixed' && _n.exists(o.offset))
                offset += _n.qs(o.offset).getBoundingClientRect()['top']
              o.targets.forEach( (t) => {
                if(_n.array(t['engage:action'].classes.add).includes('fixed') && _n.exists(o.offset))
                  offset += _n.qs(o.offset).getBoundingClientRect()['top']
              })
            } 
          }
          // if(!!trigger.getAttribute('data-offset')) offset = trigger.getAttribute('data-offset')
        }

        let delay = o.delay || 0

        let elementEdge = o.elementEdge || 'top'
        let winEdge = o.windowEdge || 'bottom'

        if(!!o.direction && Neptune.surface.direction != o.direction) return false

        if(!trigger) return false;
        const elRect = trigger.getBoundingClientRect()
        
        let winEdgeOffset = 0
        if(winEdge=='bottom') winEdgeOffset = document.documentElement.clientHeight
        if(winEdge=='bottom') winEdgeOffset = window.innerHeight

        const targets = o.targets || [{}]
        // console.log(o.direction, winEdge, elementEdge, elRect[elementEdge],(winEdgeOffset + offset))
        if(
          o.is == 'visible' && 
          elRect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
          elRect.left < (window.innerWidth || document.documentElement.clientWidth)
        ){
          if(!!o.once && o.once){
            if(!!el.getAttribute('neptune-surface-once')) return false
            el.setAttribute('neptune-surface-once','once')
          }
          setTimeout(function(){
            Neptune.surface.action(el,targets)},
            delay
          )
        } else {

          if( o.direction == 'down' && elRect[elementEdge] <= (winEdgeOffset + offset) ) {
            
            if(!!o.once && o.once){
              if(!!el.getAttribute('neptune-surface-once')) return false
              el.setAttribute('neptune-surface-once','once')
            }
            setTimeout(function(){Neptune.surface.action(el,targets)},delay)
          }

          if( o.direction == 'up' && elRect[elementEdge] > (winEdgeOffset + offset) ) {
            if(!!o.once && o.once){
              if(!!el.getAttribute('neptune-surface-once')) return false
              el.setAttribute('neptune-surface-once','once')
            }
            setTimeout(function(){Neptune.surface.action(el,targets)},delay)
          }
        }


      })
    
    })


  },
  action: (el,targets) => {

    targets.forEach( (target) => {

      Neptune.debug.info('surface:action', {element:el})
      Neptune.debug.info('surface:action', {element:target})

      let targetEl = el
      if(!!target.selector && _n.exists(target.selector)) targetEl = _n.qs(target.selector)

      if (!!target.selector && target.selector.indexOf('_self')==0 && target.selector != '_self')
        targetEl = _n.qs(target.selector.replace('_self ', ''), el)

      if (target.selector && target.selector == '_parent')
        targetEl = el.parentNode

      if (!!target.selector && target.selector.indexOf('_parent')==0 && target.selector != '_parent')
        targetEl = _n.qs(target.selector.replace('_parent ', ''), el.parentNode)

      if (target.selector && target.selector == '_grandparent')
        targetEl = el.parentNode.parentNode

      if (!!target.selector && target.selector.indexOf('_grandparent')==0 && target.selector != '_grandparent')
        targetEl = _n.qs(target.selector.replace('_grandparent ', ''), el.parentNode.parentNode)

      if(!!target['engage:action'] && !!Neptune.engage) 
        Neptune.engage.action(targetEl,target['engage:action'])

      if(!!target['periscope:view'] && !!Neptune.periscope){
        Neptune.periscope.view(targetEl,target['periscope:view'].url)
      }

      if(!!target['periscope:load'] && !!Neptune.periscope){
        Neptune.periscope.load(targetEl,target['periscope:load'])
      }

      if(!!target['liquid:load'] && !!Neptune.liquid){
        Neptune.liquid.load(target['liquid:load'].topic,target['liquid:load'].data);
      }

      // datalayer support here

    })
  }

}

window.Neptune = Neptune

export default Neptune.surface;

