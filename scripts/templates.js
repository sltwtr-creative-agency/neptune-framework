// Deprecated, please use liquid.js

import Neptune from'./neptune';
import _n from './_n';
import handlebars from '../../node_modules/handlebars'
import handlebarsIntl from '../../node_modules/handlebars-intl'
// https://formatjs.io/handlebars/
//import fetch from '../../node_modules/unfetch'
import USON from '../../node_modules/uson'
// https://github.com/burningtree/uson

import { Liquid } from '../../node_modules/liquidjs'
    
handlebarsIntl.registerWith(handlebars);

Neptune.templates = {
  items:{}
}



Neptune.templates.init = (p) => { 

  p = p || document

  Neptune.debug.info('templates:init', {el:p})
  
  let to_render = []

  // on init cycle through elements with neptune-template IN REVERSE
  _n.array(_n.qsa('[neptune-template]',p)).reverse().forEach( t => {
  
    // console.log(p,t)
    const config = USON.parse(t.getAttribute('neptune-template'),'object')

    if(!!!Neptune.templates.items[config.topic]){
      Neptune.templates.items[config.topic] = config
      Neptune.templates.items[config.topic].targets = []
    }
    // add them to the targets of the topic item, including the parent 
    
    let exists = false
    let theHTML = t.innerHTML.trim()
    t.innerHTML = ''

    Neptune.templates.items[config.topic].targets.forEach(target=>{
      target.el.innerHTML = ''
      // console.log('looking for target',target.el,t,(target.el == t))
      if(target.el == t) exists = true
    })
    if(!exists){
      Neptune.templates.items[config.topic].targets.push({el:t,template:theHTML,parent:p})
      if(!to_render.includes(config.topic)) to_render.push(config.topic)
    }
    t.setAttribute('neptune-templated','true')
    
  });

  // after all registration has occurred, load/render it up the data from the various sources including remote api calls, window reductions, and appendices
  // console.log('ok all of these have been registered, now heres what to render',to_render)
  to_render.reverse().forEach(item=>{
    Neptune.templates.load(item)
  })
  // after each item has loaded, template it.
    

};


Neptune.templates.load = (topic, data, placement) => {

  let config = Neptune.templates.items[topic]

  if(!!!config) return false

  let source = data || config.source

  if(!!!source) return false
  
  if(typeof source == 'string' && source.includes('url:')){

    fetch(source.replace('url:',''), { } )
    .then(r => r.json())
    .then(d => {
      
      if(!!config.append){
        _n.array(config.append).forEach((appendix)=>{
          if(!!Neptune.templates.items[appendix])
            d[appendix] = Neptune.templates.items[appendix].data
          if(!!appendix.split('.').reduce((o,i)=>o[i], window))
            d[appendix] = appendix.split('.').reduce((o,i)=>o[i], window)
        })
      }
      Neptune.templates.items[topic].data = d
      Neptune.templates.render(topic, null, placement)

    })

  } else {

    if(typeof source == 'string') {
      Neptune.templates.items[topic].data = source.split('.').reduce((o,i)=>o[i], window)
    } else {
      Neptune.templates.items[topic].data = data
    }
    Neptune.templates.render(topic, null, placement)

  }
}


Neptune.templates.render = (topic, callback, placement) => { 

  if(!!!Neptune.templates.items[topic] || !!!Neptune.templates.items[topic].data) return false

  const data = Neptune.templates.items[topic].data

  placement = placement || 'contents'

  _n.trigger(document,'templates:render',false,true,{topic:topic,items:Neptune.templates.items[topic],data:data})

  Neptune.templates.items[topic].targets.forEach(function(item){

    Neptune.debug.info('templates:render', {topic:topic,el:item.el,items:Neptune.templates.items[topic],data:data})

    if(item.el.getAttribute('engine') == 'handlebars') {
      var renderer = handlebars.compile(item.template);
      item.el.innerHTML = renderer(data)
      _n.trigger(document,'template:rendered',false,true,{topic:topic,el:item.el,template:item.template,data:data})

    } else {

      // console.log('render with data please!',data)

      var renderer = new Liquid();
      renderer.parseAndRender(item.template, data).then( (rendered) => {

        if( placement == 'append' ){
          // item.el.innerHTML = item.el.innerHTML + rendered
          // item.el.append(rendered)

          var temp = document.createElement('div');
          temp.innerHTML = rendered;
          _n.array(temp.childNodes).forEach( (child) => { item.el.appendChild(child) } )
          

        } else if ( placement == 'prepend' ){
          // item.el.innerHTML = rendered + item.el.innerHTML
          // item.el.prepend(rendered)

          var temp = document.createElement('div');
          temp.innerHTML = rendered;
          _n.array(temp.childNodes).reverse().forEach( (child) => { item.el.insertBefore(child, _n.qsa('[data-product-item]')[0] ) } )

        } else {
          item.el.innerHTML = rendered
        }

        _n.trigger(document,'template:rendered',false,true,{topic:topic,el:item.el,template:item.template,data:data})
        _n.trigger(item.el,'template:rendered',false,true,{topic:topic,el:item.el,template:item.template,data:data})
        
      });

    }

  })

  

}

window.Neptune = Neptune

export default Neptune.templates;
