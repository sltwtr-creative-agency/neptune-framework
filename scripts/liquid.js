import Neptune from "./neptune";
import _n from "./_n";
//import fetch from "../../node_modules/unfetch";
import USON from "../../node_modules/uson";
import { Liquid } from "../../node_modules/liquidjs";

Neptune.liquid = {
  topics: {},
};

Neptune.liquid._Liquid = Liquid;

Neptune.liquid.createEngine = () => {
  Neptune.liquid.engine = new Neptune.liquid._Liquid();

  Neptune.liquid.engine.registerFilter("handle", (v) => {
    if(!!v) return v.handle() 
  });

  Neptune.liquid.engine.registerFilter("title", (v) =>
    v.toLowerCase().split(' ').map(w=>{return (w.charAt(0).toUpperCase() + w.slice(1))}).join(' ')
  );

  Neptune.liquid.engine.registerFilter("escape", (v) =>
    v.replace(/'/g,"\\'")
  );
  
  Neptune.liquid.engine.registerFilter("newline_to_br", (v) =>
    v.replace(/(?:\r\n|\r|\n)/g, '<br/>')
  );
  Neptune.liquid.engine.registerFilter("newline_split", (v) =>
    v.split(/\r?\n/)
  );

  Neptune.liquid.engine.registerFilter(
    "file_url",
    (v) => {
      return `${window.Neptune.config.file_url_base}${v}`
    }
  );
  Neptune.liquid.engine.registerFilter(
    "asset_url",
    (v) => `${window.Neptune.config.asset_url_base}${v}`
  );
  Neptune.liquid.engine.registerFilter("t", (v) =>
    v.split(".").reduce((o, i) => o[i], window.Neptune.config.lang)
  );

  window.currencies = {
    USD:{symbol:'$'},
    CAD:{symbol:'$', label:'CAD'},
    EUR:{symbol:'', label:'€'},
    GBP:{symbol:'£'},
    JPY:{symbol:'¥', decimal:false, label:false},
    AUD:{symbol:'$', label:'AUD'},
    CRC:{symbol:'₡', label:'CRC'},
    MXN:{symbol:'$', label:'MXN'},
    ...window.liquidCurrencySchemas,
    default:{label:Shopify.currency.active}
  }

  window.currency = currencies[Shopify.currency.active] || currencies.default;

  _n.money = (v) => {
    let currency = currencies[Shopify.currency.active] || currencies.default
    if (typeof v == 'string' && v.includes('.')) v = v * 100;
    return `${currency.symbol ? currency.symbol : ``}${currency.decimal===false ? (v / 100) : (v / 100).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}${currency.label ? ` ${currency.label}`:``}`  
  }
  
  Neptune.liquid.engine.registerFilter(
    "money",
    (v) => {
      return _n.money(v)
    }
  );

  // Add new currency system alongside the old one
  _n.currency = {
    getNumberFormat: () => {
      if (Shopify.currency.active) {
        const currencyConfig = window.currencies[Shopify.currency.active]
        if (currencyConfig && currencyConfig.numberFormat) return currencyConfig.numberFormat
      }

      if (Shopify.locale && Shopify.country) return `${Shopify.locale}-${Shopify.country}`

      return navigator.language || "en-US"
    },

    symbol: () => {
      const currencyCode = Shopify.currency.active || 'USD';
      return new Intl.NumberFormat(
        _n.currency.getNumberFormat(), 
        { 
          style: 'currency', 
          currency: currencyCode,
          currencyDisplay: 'symbol'
        }
      ).format(0).replace(/[0-9.,]/g, '').trim();
    },
  
    code: () => {
      return Shopify.currency.active || 'USD';
    },
  
    format: (n) => {
      return new Intl.NumberFormat(
        _n.currency.getNumberFormat(), 
        { 
          style: 'decimal',
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }
      ).format(n / 100);
    },
  
    money: (n) => {
      n = (typeof n == 'string' && n.includes('.')) ? n*100 : n;
      
      if (!window.__currencyFormat) {
        return Intl.NumberFormat(
          _n.currency.getNumberFormat(), 
          { 
            style: 'currency', 
            currency: _n.currency.code()
          }
        ).format(n / 100);
      }
      
      // Get our formatted number
      const formattedNumber = _n.currency.format(n);
      
      // Use the window.__currencyFormat pattern directly, just replace the placeholder amount
      return window.__currencyFormat.replace(/[0-9.,]+/, formattedNumber);
    }
  };
  
  Neptune.liquid.engine.registerFilter("currency", (v) => _n.currency.money(v));

  Neptune.liquid.engine.registerTag("random", {
    parse: function(tagToken, remainTokens) {
      this.str = tagToken.args; // name
    },
    render: async function(scope, hash) {
      var str = await this.liquid.evalValue(this.str, scope); // 'alice'
      return _n.random();
    },
  });
  
  Neptune.liquid.engine.registerTag("import", {
    parse: function(tagToken, remainTokens) {
      this.str = tagToken.args; // name
    },
    render: async function(scope, hash) {
      var str = await this.liquid.evalValue(this.str, scope); // 'alice'
      if(_n.exists(str)) return [..._n.qsa(str)].reduce((o,i) => o + i.innerHTML, '')
    },
  });
  
  for (let b in _n.breakpoints){
    Neptune.liquid.engine.registerFilter(b, (current, modified) => {
      if(window.outerWidth >= _n.breakpoints[b]) return modified;
      return current;
    })
  }

  Neptune.liquid.engine.registerFilter("fraction", (v) => _n.fraction(v));
  Neptune.liquid.engine.registerFilter(
    "feet",
    (v) => `${Math.floor(v / 12)}' ${(v %= 12)}"`
  );

  _n.trigger(document, "liquid:engine", false, true, {});

  return Neptune.liquid.engine;
};

Neptune.liquid.init = (p) => {
  
  Neptune.liquid.items = Neptune.liquid.topics;

  p = p || document;

  if(!Neptune.liquid.engine)
    Neptune.liquid.engine = Neptune.liquid.createEngine();

  // if (!_n.exists("[neptune-template],[neptune-liquid]", p)) return false;

  Neptune.debug.info("liquid:init", { el: p });

  let to_render = [];

  // on init cycle through elements with neptune-template IN REVERSE
  _n.array(_n.qsa("[neptune-template],[neptune-liquid]", p))
    .reverse()
    .forEach((t) => {
      
      let att = "neptune-liquid";
      if (t.getAttribute("neptune-template")) att = "neptune-template";
      
      let config
      try{
        config = USON.parse(t.getAttribute(`${att}`), "object");
      } catch(err){
        console.log('USON Parse Error', t, t.getAttribute(`${att}`), err) 
      }

    if(!!!Neptune.liquid.topics[config.topic]){
      Neptune.liquid.topics[config.topic] = config
      Neptune.liquid.topics[config.topic].targets = []
    }
    // add them to the targets of the topic item, including the parent 

    if(!!config.append) {
      if(!Neptune.liquid.topics[config.topic].append) Neptune.liquid.topics[config.topic].append = [];
      Neptune.liquid.topics[config.topic].append = _n.array(Neptune.liquid.topics[config.topic].append).concat(config.append)
    }

    t.topic = config.topic
    // add the topic to the element
    
    let exists = false
    let template = t
    
    try {
      if(Array.from(t.children).find(c=>c.matches('template:not([type="skeleton"]),script[type="liquid/template"]'))){
        // console.log()
        template=Array.from(t.children).find(c=>c.matches('template:not([type="skeleton"]),script[type="liquid/template"]'))
      } else {
        console.warn(`This neptune-liquid element (${t.topic}) needs a template tag:`, t)
      }
    } catch(err){}
    
    let theHTML = template.innerHTML.trim().replace(/&gt;/g, '>').replace(/&lt;/g, '<').replace(/&amp;/g, '&')
    
    let skeleton = '';

    // t.innerHTML = skeleton;

    Neptune.liquid.topics[config.topic].targets.forEach(target=>{
      // target.el.innerHTML = ''
      // console.log('looking for target',target.el,t,(target.el == t))
      if(target.el == t) exists = true
    })

    if(!exists){
      Neptune.liquid.topics[config.topic].targets.push({el:t,template:theHTML,parent:p})
      if(!to_render.includes(config.topic)) to_render.push(config.topic)
    }
    t.setAttribute('neptune-templated','true')

      if (!!config.subscribe) {
        document.addEventListener("templates:render", (e) => {
          if (e.detail.info.topic == config.subscribe)
            Neptune.liquid.load(config.topic);
        });
      }
    });

  // after all registration has occurred, load/render it up the data from the various sources including remote api calls, window reductions, and appendices
  to_render.reverse().forEach((item) => {
    Neptune.liquid.load(item);
  });
  
  Neptune.liquid.attributes.init();

};

Neptune.liquid.load = (topic, data, placement) => {

  let config = Neptune.liquid.topics[topic];

  if (!!!config) return false;

  return new Promise((res, rej) => {

    data = data || config.data;

    let source = data || config.source;

    if (typeof source == "string" && source.includes("url:")) {

      if(!!config.cache) {
        if (window[`${config.cache}Storage`].getItem(`Liquid__${topic}`)) {
          Neptune.liquid.topics[topic].data = JSON.parse(window[`${config.cache}Storage`].getItem(`Liquid__${topic}`))
          Neptune.liquid.render(topic, null, placement);
          res(); return;
        }
      }
      
      fetch(source.replace("url:", ""), {})
        .then((r) => r.json())
        .then((d) => {
          if (!!config.append) {
            _n.array(config.append).forEach((appendix) => {
              if (!!Neptune.liquid.topics[appendix])
                d[appendix] = Neptune.liquid.topics[appendix].data;
              if (!!appendix.split(".").reduce((o, i) => o[i], window))
                d[appendix] = appendix.split(".").reduce((o, i) => o[i], window);
            });
          }
          Neptune.liquid.topics[topic].data = d;
          Neptune.liquid.render(topic, null, placement);
          if(!!config.cache) {
            if (!window[`${config.cache}Storage`].getItem(`Liquid__${topic}`)) {
              window[`${config.cache}Storage`].setItem(`Liquid__${topic}`, JSON.stringify(d))
            }
          }
          res()
        });
    } else {
      if (typeof source == "string" && !source.includes("_self")) {
        Neptune.liquid.topics[topic].data = source
          .split(".")
          .reduce((o, i) => o[i], window);
      } else {
        if (!!data) {
          Neptune.liquid.topics[topic].data = data;
        } else {
          Neptune.liquid.topics[topic].data = window;
        }
      }
      res( Neptune.liquid.render(topic, null, placement) );
    }

  })
};

Neptune.liquid.render = async (topic, callback, placement, clearPermanent) => {

  if (!!!Neptune.liquid.topics[topic] || !!!Neptune.liquid.topics[topic].data)
    return false;

  let data = Neptune.liquid.topics[topic].data;

  if (Array.isArray(data)) data = { sourceData: data };

  if (!!Neptune.liquid.topics[topic].map)
    data = _n.map(data, Neptune.liquid.topics[topic].map);

  let config = Neptune.liquid.topics[topic];

  placement = placement || config.placement || "contents";

  _n.trigger(document, "templates:render", false, true, {
    topic: topic,
    topics: Neptune.liquid.topics[topic],
    data: data,
  });

  Neptune.liquid.topics[topic].targets = Neptune.liquid.topics[
    topic
  ].targets.filter((target) => {
    return true;
    // return target.el.parent.includes(target.el)
  });

  Neptune.liquid.topics[topic].targets.forEach(function(item) {
    Neptune.debug.info("templates:render", {
      topic: topic,
      el: item.el,
      topics: Neptune.liquid.topics[topic],
      data: data,
    });

    // here we look for data that is local to the element using "_self" or ._parent(qs)
    if (
      !!Neptune.liquid.topics[topic].source &&
      typeof Neptune.liquid.topics[topic].source == "string" &&
      Neptune.liquid.topics[topic].source.includes("_self")
    ) {
      data = eval(
        Neptune.liquid.topics[topic].source.replace("_self", "item.el")
      );
    }
    if (
      !!Neptune.liquid.topics[topic].source &&
      !!Neptune.liquid.topics[topic].source._parent
    ) {
      data = _n.parents(item.el, Neptune.liquid.topics[topic].source._parent);
    }

    if (
      !!Neptune.liquid.topics[topic].source &&
      !!Neptune.liquid.topics[topic].source.selector
    ) {
      data = _n.qs(Neptune.liquid.topics[topic].source.selector);
    }

    // // here we look to
    // if(!!Neptune.liquid.topics[topic].source && typeof Neptune.liquid.topics[topic].source == 'string' && Neptune.liquid.topics[topic].source.includes('qs(')){
    //   data = _n.qs(Neptune.liquid.topics[topic].source.split('qs(')[1].split(')')[0])
    //   console.log(Neptune.liquid.topics[topic].source,data)
    // }

    if (!!Neptune.liquid.topics[topic].append) {
      _n.array(Neptune.liquid.topics[topic].append).forEach((appendix) => {
        if (!!Neptune.liquid.topics[appendix])
          data[appendix] = Neptune.liquid.topics[appendix].data;
        if (!!appendix.split(".").reduce((o, i) => o[i], window))
          data[appendix] = appendix.split(".").reduce((o, i) => o[i], window);
      });
    }

    new Promise((res, rej) => {
      
      if(!_n.hooks) res(data);
      
      return _n.hooks.process('liquid:prerender', data, {topic:topic,item:item}).then(processedData => {
        res(processedData)
      })

    }).then(data => Neptune.liquid.engine.parseAndRender(item.template, data))
      .then(rendered => {

        if (placement == "append") {
          // item.el.innerHTML = item.el.innerHTML + rendered
          // item.el.append(rendered)

          var temp = document.createElement("div");
          temp.innerHTML = rendered;
          _n.array(temp.childNodes).forEach((child) => {
            item.el.appendChild(child);
          });
        } else if (placement == "prepend") {
          // item.el.innerHTML = rendered + item.el.innerHTML
          // item.el.prepend(rendered)

          var temp = document.createElement("div");
          temp.innerHTML = rendered;
          _n.array(temp.childNodes)
            .reverse()
            .forEach((child) => {
              item.el.insertBefore(child, item.el.firstChild);
            });
        } else if (placement == "after") {
          var temp = document.createElement("div");
          temp.innerHTML = rendered;
          _n.siblings(item.el).forEach(sib => {
            if (sib.hasAttribute('neptune-liquid-rendered')) sib.remove()
          })
          _n.array(temp.childNodes).forEach((child) => {
            if (child.setAttribute) child.setAttribute('neptune-liquid-rendered', true)
          });
          item.el.insertAdjacentHTML('afterend', temp.innerHTML)
        } else {
          item.el.classList.add("liquid-rendering");

          let focused = false;

          if (!clearPermanent) {
            var preserved = {};
            _n.qsa("[neptune-permanent]", item.el).forEach((np) => {
              if(!np.getAttribute("neptune-permanent")) return false;
              np.setAttribute(
                "permanent",
                np.getAttribute("neptune-permanent")
              );
              preserved[np.getAttribute("neptune-permanent")] = np;
            });
          }

          try {
            if (item.el.contains(document.activeElement)) {
              focused = _n.rqs(document.activeElement);
            }
          } catch (err) {}

          if(item.el.innerHTML != rendered)
            item.el.innerHTML = rendered;

          if (!clearPermanent) {
            _n.qsa("[neptune-permanent]", item.el).forEach((np) => {

              if(!!np.parentNode.nodeName && !!preserved[np.getAttribute("neptune-permanent")]){
                np.parentNode.replaceChild(
                  preserved[np.getAttribute("neptune-permanent")],
                  np
                );
              }
              
            });
          }

          if (!!focused && _n.exists(focused)) _n.qs(focused).focus();

          item.el.classList.remove("liquid-rendering");

        }

      _n.trigger(document,'template:rendered',false,true,{topic:topic,el:item.el,template:item.template,data:data})
      _n.trigger(item.el,'liquid:rendered',false,true,{topic:topic,el:item.el,template:item.template,data:data})

      _n.qsa("script", item.el).forEach((script) => {
        try {
          if (!!script.innerText) eval(script.innerText);
        } catch (err) {}
      });
              
    });

  })

}



Neptune.liquid.splice = (topic, path, data) => {
  console.log(topic, path, data)
}


Neptune.liquid.attributes = {
  
  toRender:[],
  resizer: false,
  
  init: p => {
    Neptune.liquid.attributes.toRender = []
    _n.xp('//*[@*[starts-with(name(), "n:")]]').forEach(el=>{

      el.n = {}
      
      for (var i = 0, atts = el.attributes, n = atts.length, arr = []; i < n; i++){
        if(atts[i].nodeName.startsWith('n:')){
          el.n[atts[i].nodeName.split('n:')[1]] = atts[i].nodeValue
          Neptune.liquid.attributes.toRender.push( { el:el, att:atts[i].nodeName.split('n:')[1], liquid: atts[i].nodeValue } )
        }
      }

    })
    Neptune.liquid.attributes.render()

    if(!Neptune.liquid.attributes.resizer){
      window.addEventListener('resized',e=>{
        Neptune.liquid.attributes.render(
          Neptune.liquid.attributes.toRender.filter(r=>['sm:','md:','lg:','xl:','2xl:'].some(v=>r.liquid.includes(v)))
        )
      })
      Neptune.liquid.attributes.resizer = true
    }

    
  },

  render: (R) => {

    R = R || Neptune.liquid.attributes.toRender 

    R.forEach(r=>{
      let data = window
      if( _n.parents(r.el, '[neptune-liquid]') ){ 
        data = Neptune.liquid.topics[_n.parents(r.el, '[neptune-liquid]').topic].data
      }
      Neptune.liquid.engine.parseAndRender(
        r.liquid.includes('{') ? r.liquid : `{{ ${r.liquid} }}`, 
        data
      ).then(rendered=>{
        
        if(rendered.trim() == '_remove') 
          return r.el.removeAttribute(r.att);

        r.el.setAttribute(
          r.att, 
          rendered
        )
        
      });
      
    })
    

  }

}

document.addEventListener("template:rendered", Neptune.liquid.attributes.init)

Neptune.templates = Neptune.liquid; // legacy support
window.Neptune = Neptune;

export default Neptune.liquid;
