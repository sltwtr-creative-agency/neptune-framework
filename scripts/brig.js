import Neptune from "./neptune";
import _n from "./_n";

Neptune.brig = {
  
  focusable:
    'a[href]:not([disabled],[aria-hidden="true"]), button:not([disabled],[aria-hidden="true"]), textarea:not([disabled],[aria-hidden="true"]), input[type="text"]:not([disabled],[aria-hidden="true"]), input[type="radio"]:not([disabled],[aria-hidden="true"]), input[type="checkbox"]:not([disabled],[aria-hidden="true"]), input[type="search"]:not([disabled],[aria-hidden="true"]), select:not([disabled],[aria-hidden="true"]), [tabindex]:not([aria-hidden="true"])',

  init: p => {
    p = p || document;

    _n.qsa("[neptune-brig]", p).forEach( el => {

      if(!!el.brig) return false;
      
      let _brig = {};

      try {
        _brig = USON.parse(
          el
            .getAttribute("neptune-brig")
            .replace(/'/g, '"')
            .replace(/`/g, "'")
            .replace(/</g, "[")
            .replace(/>/g, "]"),
          "array"
        )[0];
      } catch (error) {
        _brig = {}
      }

      _brig.keys = _n.array(_brig.keys || ["Tab", 9]);
      _brig.targets = !!_brig.targets ? _n.array(_brig.targets) : Neptune.brig.focusable;
      _brig.method = _brig.method || "focus";
      _brig.event = _brig.event || "keydown";

      el.addEventListener(_brig.event, (e) => {
        if (
          _n.exists("[neptune-brig]", el) &&
          _n.qs("[neptune-brig]", el).contains(document.activeElement)
        )
          return false;
        if (
          (_brig.keys.includes(e.key) || _brig.keys.includes(e.keyCode)) &&
          (document.activeElement == el ||
            (el.contains(document.activeElement) &&
              document.activeElement.matches(_brig.targets)))
        ) {

          let action = false;
          if (e.keyCode == 9) action = "next";
          if (e.keyCode == 39) action = "next";
          if (e.keyCode == 40) action = "next";
          if (e.shiftKey && e.keyCode == 9) action = "prev";
          if (e.keyCode == 37) action = "prev";
          if (e.keyCode == 38) action = "prev";

          _brig.target_elements = _n
            .qsa(_brig.targets, el)
            .filter((child) => child.closest("[neptune-brig]") === el);
          _brig.currentIndex = _brig.target_elements.indexOf(
            document.activeElement
          );

          if (action) {
          e.preventDefault();
            if (action == "next") {
              if (_brig.currentIndex == _brig.target_elements.length - 1) {
                _brig.target_elements[0].focus();
              } else {
                _brig.target_elements[_brig.currentIndex + 1].focus();
              }
            }
            if (action == "prev") {
              if (_brig.currentIndex == [0]) {
                _brig.target_elements[_brig.target_elements.length - 1].focus();
              } else {
                _brig.target_elements[_brig.currentIndex - 1].focus();
              }
            }
          }
        }
        return true;
      });

      el.brig = _brig;

    });
  },

  setTabbable: (element, isTabbable) => {
    // Set the tabindex of an element and any of its children
    const setTab = (el) => {
      if (isTabbable) {
        el.setAttribute('tabindex', 0)
      } else {
        el.setAttribute('tabindex', -1)
      }
    }

    setTab(element)

    _n.qsa(Neptune.brig.focusable, element).forEach(focusableElement => {
      setTab(focusableElement)
    })
  }
};

window.Neptune = Neptune;

export default Neptune.brig;
