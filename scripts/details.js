import Neptune from "./neptune";
import _n from "./_n";

Neptune.details = {

	// types: [
	// 	'accordion', // expands current collapses others
	// 	'popover', // hides on click anywhere outside
	// 	'tabs' // same as accordion i suppose, not currently supported
	// ],

	init: p => {
		
		p == p || document;

		const activateAccordionFromURL = () => {
            const urlParams = new URLSearchParams( window.location.search );
            const accordionParam = urlParams.get('accordion');
            
            if ( accordionParam ) {
                const targetAccordion = _n.qs( `.accordion button[data-id="${accordionParam}"]`, p );
                if ( targetAccordion ) {
                    targetAccordion.click();
                }
            }
        }; 

		activateAccordionFromURL();
		
		document.addEventListener('click', e => {
				
			_n.qsa('details[type]').forEach( details => {
				
				details.type = details.getAttribute('type')

				if( details.type=='popover' && !details.contains(e.target) ){
					details.removeAttribute('open')
				}

				if( details.type=='modal' && !details.contains(e.target) ){
					details.removeAttribute('open')
				}

				if( details.type=='accordion' && details.contains(e.target) ){
					_n.qsa(`details[type="accordion"][group="${details.getAttribute('group')}"]`).filter(oe=>oe != details).map(oe=>oe.removeAttribute('open'))
				}

			})

		})

	}
}

window.Neptune = Neptune;

export default Neptune.details;