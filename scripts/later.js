 // This Utility Controller is used for deferring tracking of events to the next page load.
 // Neptune.later.store(event) stores the event
 // Neptune.later.recall() executes any events stored for page load and triggers a 'Neptune:Later' event
Neptune.later = {
   sessionKey: 'neptune_later',
   registry: {
     default: (event, type) => ({
      target: event.target.outerHTML,
      detail: event.detail,
      location: window.location,
      type: type
     })
     // Register a callback function that returns any additional data
     // type: [(event) => {
     //   const addtlData = {}
     //
     //   addtlData.custom_value = event.target.closest('section').id
     //
     //   return addtlData
     // }]
   },

   init: () => {
    Neptune.later.recall()
    _n.trigger(window,'NeptuneLater:ready')
   },

  // Store the event to be recalled
   store: (event, type)  => {
    const events = []
    const storedEvents = Neptune.later.getEvents()
    let addtlData = {}

    if (!event) return false
    if (storedEvents) events.concat(storedEvents) 

    if (!!type) addtlData.type = type

    if (!!type && !!Neptune.later.registry[type]) {
       Neptune.later.registry[type].forEach(assignment => {
        addtlData = {...addtlData, ...assignment(event)}
       })
     }

     events.push({
      ...Neptune.later.registry.default(event),
      ...addtlData
     })
     window.sessionStorage.setItem(Neptune.later.sessionKey, JSON.stringify(events))
   },

   // Replay all the stored events
   recall: () => {
     const storedEvents = Neptune.later.getEvents()

     if (!storedEvents) return false

     storedEvents.forEach(event => {
      _n.trigger(window,'Neptune:Later',false,true,{event:event})
     })

     return window.sessionStorage.removeItem(Neptune.later.sessionKey)
   },

   // Get any stored events
   getEvents: () => {
     const storedEvents = window.sessionStorage.getItem(Neptune.later.sessionKey)

     if (!storedEvents) return false
     return JSON.parse(storedEvents)
   },

   // Register custom data for specific events
   register: (type, assignment) => {
     if (!!Neptune.later.registry[type]) return Neptune.later.registry[type].push(assignment)

     Neptune.later.registry[type] = [assignment]
   }
}

window.Neptune = Neptune

export default Neptune.later
