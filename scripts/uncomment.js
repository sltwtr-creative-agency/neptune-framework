Neptune.uncomment = {
  init: p => {
    
    p = p || document;
    
    _n.qsa('[neptune-uncomment]', p).forEach( el => {

      if(el.getAttribute('neptune-uncomment')==='')
        Neptune.uncomment.uncomment(el);
      
      el.addEventListener(el.getAttribute('neptune-uncomment'), () => {

        Neptune.uncomment.uncomment(el);

      });
    
    })

    window.addEventListener('scroll',_n.debounce(e=>{
      Neptune.uncomment.visible()
    },500))
    window.addEventListener('scroll',_n.throttle(e=>{
      Neptune.uncomment.visible()
    },500))

  },
  uncomment: el => {

    if(el.innerHTML.includes('<!--'))
      el.innerHTML = el.innerHTML.replace('<!--','').replace('-->','');
  },

  visible: () =>{
    _n.qsa('[neptune-uncomment="visible"]').forEach(el=>{
      if(_n.inview(el,100,{edge:'top'})){
        el.removeAttribute('neptune-uncomment')
        Neptune.uncomment.uncomment(el);
      }
    })
  }
}

window.Neptune = Neptune

window.addEventListener('load', e=>{
  Neptune.uncomment.visible()
})

export default Neptune.uncomment;

