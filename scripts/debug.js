import Neptune from'./neptune';
import _n from './_n';

Neptune.debug = {
  levels: [],
  init: () => {
    if(_n.exists('[neptune-debug]')){
      Neptune.debug.levels = _n.qs('[neptune-debug]').getAttribute('neptune-debug').split(',').map((l)=>{
        return l.trim()
      })
    }
  },
  info: ( subject, data, msg ) => {
    Neptune.debug.log( 'info', subject, data, msg)
  },
  warn: ( subject, data, msg ) => {
    Neptune.debug.log( 'warn', subject, data, msg)
  },
  error: ( subject, data, msg ) => {
    Neptune.debug.log( 'error', subject, data, msg)
  },
  log:( type, subject, data, message ) => {

    let log = false;

    if(!!data.el){
      _n.qsa('[neptune-debug]').forEach((debugEl)=>{
        if( 
          debugEl.getAttribute('neptune-debug').includes(type) && 
          (data.el == window || debugEl == data.el || debugEl.contains( data.el ) )
        ) {
          log = true
        }
      })
    } else {
      log = true
      // may want to remove this at some point here soon
    }
    
    if(!log) return false;
    
    message = message || ''
    type = type || 'info'
    data = data || {}
    subject = subject || ''

    if(!!!Neptune.debug.levels || !Neptune.debug.levels.includes(type)) return false

    const badgestyles = {
      'info': 'color: white; background-color: blue; border-radius: 0 3px 3px 0;font-size:9px;padding:2px;',
      'warn': 'color: white; background-color: #ffbc00; border-radius: 0 3px 3px 0;font-size:9px;padding:2px;',
      'error': 'color: white; background-color: red; border-radius: 0 3px 3px 0;font-size:9px;padding:2px;'
    }
    console.log(
      '%c Neptune %c '+subject+' ',
      'color: blue;background:white;font-style:italic;border-radius: 3px 0 0 3px;font-size:9px;padding:2px;',
      badgestyles[type],
      message,
      data
    )
  }
}

window.Neptune = Neptune

export default Neptune.debug;
