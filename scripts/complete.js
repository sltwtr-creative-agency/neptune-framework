Neptune.complete = {
  init: () => {
    _n.qsa('[oncomplete]:not(.completeable)').forEach(input=>{
      input.classList.add('completeable')
        input.typingTimer = input.typingTimer || {} 
        input.addEventListener('keydown', event=>{
          clearTimeout(input.typingTimer)
        })
        input.addEventListener('keyup', event=>{
          let _this = { value: input.value };
          clearTimeout(input.typingTimer)
          input.typingTimer = setTimeout(function(){
              eval(input.getAttribute('oncomplete').replace(/this/g, '_this'))
          },500);
        })
    })
  }
}

window.Neptune = Neptune

export default Neptune.complete