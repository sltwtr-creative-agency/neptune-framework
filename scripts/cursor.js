
import Neptune from './neptune'
import _n from './_n'

Neptune.cursor = {

  init: (p) => {
    p = p || document;

    _n.qsa("[neptune-cursor]", p).forEach((el) => {
      let _cursor = {};

      try {
        _cursor = USON.parse(
          el
            .getAttribute("neptune-cursor")
            .replace(/'/g, '"')
            .replace(/`/g, "'")
            .replace(/</g, "[")
            .replace(/>/g, "]"),
          "array"
        )[0];
      } catch (error) {
        _cursor = {}
      }

      let cursor = Neptune.cursor.createCursor(el, _cursor)

      if(cursor) Neptune.cursor.attachListener(el, cursor)

    })
  },

  createCursor: (c, settings) => {
    
    const cursor = settings.selector ? _n.qs(settings.selector, c) : document.createElement("span")

    //console.log('cursor',cursor)
    if(!cursor) return false;

    if (!!!settings.selector) {
      cursor.innerText = settings.text || ""
    }
    c.style.position = "relative"
    c.style.cursor = "none"
    c.appendChild(cursor)
    cursor.style.position = "absolute"
    cursor.style.top = "0"
    cursor.style.left = "0"
    cursor.style.display = "none"

    return cursor
  },

  attachListener: (c, cursor) => {

    c.addEventListener('mouseenter', (e) => {cursor.style.display = "block"})

    c.addEventListener('mousemove', (e) => {
      let parent = _n.parents(e.target, "[neptune-cursor]").getBoundingClientRect()
      let x = e.clientX - parent.x
      let y = e.clientY - parent.y

      cursor.style.transform = `translate(${x}px, ${y}px)`
    })
    
    c.addEventListener('mouseleave', (e) => {cursor.style.display = "none"})

    _n.qsa('[class*="cursor-"]', c).forEach(nc=>{
      nc.addEventListener('mouseover', (e) => {cursor.style.display = "none"})
      nc.addEventListener('mouseleave', (e) => {cursor.style.display = "block"})
    })
  }

}
window.Neptune = Neptune

export default Neptune.cursor;
