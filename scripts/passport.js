/*
Passport is an engine for storing, retreivibg, and managing information about a user to the session, 
browser, and their shopify user account 
Requires Neptune-passport.liquid
*/

Neptune.passport = {

  information: {},

  init: () => {
    Neptune.passport.load()
    if(!!!sessionStorage.getItem('NEPTUNE_PASSPORT_SYNC')){
      Neptune.passport.sync()
    }
  },

  load: () => {
    if(!!sessionStorage){
      Object.keys(sessionStorage).forEach(function(item){
         if ( !!item && item.includes('Neptune:') )
          Neptune.passport.information[item.split('Neptune:')[1]] = JSON.parse(sessionStorage.getItem(item))
      });
    }
    if(!!localStorage){
      Object.keys(localStorage).forEach(function(item){
         if ( !!item && item.includes('Neptune:') )
          Neptune.passport.information[item.split('Neptune:')[1]] = JSON.parse(localStorage.getItem(item))
      });
    }
    
    try{
      // render to liquid from passport
      if(!!Neptune.liquid){
        Object.keys(Neptune.passport.information).forEach(topic=>{
          if(!!Neptune.liquid.items[topic]) Neptune.liquid.load(topic)
        })
      }
    } catch(err) {}
    
  },

  register: (topic,type,_default) => {

    if(!!Neptune.passport.information[topic]) return false;

    Neptune.debug.info('passport:register', {el:window, topic:topic,type:type,_default:_default})
    
    Neptune.passport.information[topic] = {
      type:type,
      updated:'',
      data:_default,
      persist: (type.includes('persist'))
    }

  },

  store: (topic,data,type='local',compared) => {

    Neptune.debug.info('passport:store', {el:window, topic:topic,data:data,type:type})
    
    type = type || 'local'
    if(!!Neptune.passport.information[topic]) type = Neptune.passport.information[topic].type

    if(!!!Neptune.passport.information[topic]) Neptune.passport.register(topic,type)

    Neptune.passport.information[topic].updated = new Date(Date.now()).toISOString()
    Neptune.passport.information[topic].data = data

    Neptune.passport.information[topic].compared = compared || false
    
    if (type == 'session') {
      sessionStorage.setItem('Neptune:'+topic, JSON.stringify(Neptune.passport.information[topic]))
    } else {
      localStorage.setItem('Neptune:'+topic, JSON.stringify(Neptune.passport.information[topic]))
    }

    if (type.includes('persist') && !compared) Neptune.passport.sync()

  },

  read: (topic) => {
    
    if(!!topic && !!Neptune.passport.information[topic]) 
      return Neptune.passport.information[topic].data
      
    return false

  },

  sync: () => {

    if(!!!window.customer) {
      //console.log('No logged in customer')
      Neptune.debug.warn('passport:sync', {el:window}, 'No logged in customer')
      return false
    }

    if(!!!Shopify.shop) {
      Neptune.debug.warn('passport:sync', {el:window}, 'No permanent_domain')
      return false
    }

    Neptune.debug.info('passport:sync', {el:window})
    sessionStorage.setItem('NEPTUNE_PASSPORT_SYNC',true)

    fetch(`https://apps.sltwtr.com/shopify/neptune/src/public/passport?shop=${Shopify.shop}&customer_id=${window.customer.id}`, {
      method: 'GET'
    })
    .then(r => r.json())
    .then(information => {
      Neptune.debug.info('passport:get', {el:window, information:information})
      if(typeof information == 'object'){
        for(let topic in information) {
          
          let remote_item = information[topic]

          if(!!Neptune.passport.information[topic]) {

            if(Neptune.passport.information[topic].type.includes('/merge')){

              if(Array.isArray(Neptune.passport.information[topic].data)) {
                if(Neptune.passport.information[topic].updated < remote_item.updated){
                  Neptune.passport.information[topic].data = [...remote_item.data, ...Neptune.passport.information[topic].data]
                } else {
                  Neptune.passport.information[topic].data = [...Neptune.passport.information[topic].data, ...remote_item.data]
                }
                
                Neptune.passport.information[topic].data = Neptune.passport.information[topic].data.filter((value, index, self)=>{
                  let _self = self,
                    _value = value,
                    _reducer = Neptune.passport.information[topic].type.split('merge:')[1];
                  if(!!_reducer){
                    _self = self.map(_i=>_i[_reducer])
                    _value = value[_reducer]
                  }
                  return _self.indexOf(_value) === index;
                })

              }

            } else {

              if(Neptune.passport.information[topic].updated < remote_item.updated){
                Neptune.passport.information[topic].data = remote_item.data
                Neptune.passport.information[topic].updated = remote_item.updated
                Neptune.passport.information[topic].compared = true
              }

            }
            localStorage.setItem('Neptune:'+topic, JSON.stringify(Neptune.passport.information[topic]))

          } else {
            // create new and put in localStorage
            Neptune.passport.store(topic,remote_item.data,'persist',true)
            localStorage.setItem('Neptune:'+topic, JSON.stringify(Neptune.passport.information[topic]))
          }
          Neptune.passport.load()
        }
      }

      // now run through all of the local passport items and if they have not been compared
      for(let topic in Neptune.passport.information) { 
        let local_item = Neptune.passport.information[topic]
        if(!!!local_item.compared) {
          Neptune.debug.info('passport:send', {el:window, topic:topic, information:local_item})
          fetch(`https://apps.sltwtr.com/shopify/neptune/src/public/passport/${topic}?shop=${Shopify.shop}&customer_id=${window.customer.id}`, {
            method: 'POST',
            body: JSON.stringify(local_item.data)
          })
        }
      }

    })
  }
}

window.Neptune = Neptune

export default Neptune.passport;
