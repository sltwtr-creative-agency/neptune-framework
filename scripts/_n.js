var _n = _n || {};

/* Query Selectors */
_n.qsa = (cs, ps, map) => {
  ps = ps || document;
  if (typeof ps == 'function') {
    map = ps
    ps = document
  } 
  const all = _n.array(ps.querySelectorAll(cs));
  if(!!map){
    return all.map(map)
  }
  return all;
};

_n.qs = (cs, ps, map) => {
  ps = ps || document;
  return _n.qsa(cs, ps, map)[0];
};

_n.xp = function(xpathToExecute){
  var result = [];
  var nodesSnapshot = document.evaluate(xpathToExecute, document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null );
  for ( var i=0 ; i < nodesSnapshot.snapshotLength; i++ ){
    result.push( nodesSnapshot.snapshotItem(i) );
  }
  return result;
}


/* All in the family */
_n.parents = (el, qs, is) => {
  let ignore_self = is || false;
  if (el.matches("body") || el.matches("html")) return false;
  if (!el.matches(qs) && el.parentNode != null) {
    return _n.parents(el.parentNode, qs);
  } else if (el.matches(qs) && ignore_self) {
    return _n.parents(el.parentNode, qs);
  } else {
    return el;
  }
};
_n.sibling = (el, qs) => {
  return _n.array(el.parentNode.children).find(s=>s.matches(qs) && s!=el);
}
_n.siblings = (el, qs) => {
  if(!!qs){
    _n.array(el.parentNode.children).filter(s=>s.matches(qs) && s!=el);
  }
  return _n.array(el.parentNode.children).filter(s=>s!=el);
};
_n.cousin = (el, pqs, cqs) => {
  return _n.qs(cqs, _n.parents(el,pqs))
}
_n.cousins = (el, pqs, cqs) => {
  return _n.qsa(cqs, _n.parents(el,pqs))
}

_n.s = (qs, el) => {
  el = el || document.body
  if (el.matches("html")) return [];
  if(el.matches(qs)) {
    return el;
  } else if (_n.qsa(qs,el).length > 0) {
    return _n.qsa(qs,el);
  } else if (_n.qsa(qs,el).length===0 && !el.matches(qs) && !!el.parentNode) {
    return _n.s(qs,el.parentNode);
  }
  return []
};


_n.last = (qs, p) => {
  p = p || document;
  return _n.array(_n.qsa(qs, p)).reverse()[0];
}; 

_n.exists = (qs, p) => {
  try{
    return (_n.qsa(qs, p).length > 0)
  } catch(err) {
    return false
  }
  return false
};

_n.index = el => {
  var children = el.parentNode.childNodes;
  var num = 0;
  for (var i = 0; i < children.length; i++) {
    if (children[i] == el) return num;
    if (children[i].nodeType == 1) num++;
  }
  return -1;
}

/* Reverse QuerySelector: creates queryselector from element */
_n.rqs = (el, qs) => {
  
  qs = qs || ''
  
  let selector = el.nodeName.toLowerCase()
  if(!!el.id) {
    selector += `#${el.id}`;
  }
  const classes = _n.array(el.classList).filter(c=>![':','/'].some(s=>c.includes(s)))
  if(!el.id && classes.length){
    selector += `.${classes.join('.')}`
  }
  try{
    if(!el.id && !!selector && !!el.parentElement && _n.qsa(selector, el.parentNode).length > 1) {
      selector += `:nth-child(${_n.index(el)+1})`
    }
  } catch(err) {
    // console.log(el, err)
  }
  
  qs = `${selector} ${qs}`
  
  if (el.matches("body") || el.matches("html")) {
    return qs;
  } else {
    return _n.rqs(el.parentNode, qs)
  }
};

// sequential functions
_n.flow = funcs => (...args) => funcs.reduce((prev, fnc) => [fnc(...prev)], args)[0];

_n.hooks = {

  registry: {},

  register: (event, func) => {
    // func is a function must return a promise
    if(!_n.hooks.registry[event]) _n.hooks.registry[event] = [];
    _n.hooks.registry[event].push(func)
    return true
  },

  process: (event, data, config) => {
    let result = Promise.resolve();
    (_n.hooks.registry[event]||[]).forEach((hooked, index) => {
      result = result.then(() =>
        new Promise(resolve => {
          resolve(hooked(data, config));
        })
      );
    });
    return result.then(() => data);
  }

}


/*
much better version
_n.flow = funcs => (...args) => {
  return funcs.reduce((prev, fnc) => {
    return prev.then(result => {
      return Promise.resolve(fnc(result));
    });
  }, Promise.resolve(args[0]));
};
*/

/* Arrays and Objects */
_n.array = function(val) {
  if(Array.isArray(val))
    return val
  if(NodeList.prototype.isPrototypeOf(val) || HTMLCollection.prototype.isPrototypeOf(val) || DOMTokenList.prototype.isPrototypeOf(val)) 
    return Array.from(val)
  if(typeof val == 'string' || typeof val == 'number') 
    return [val]
  if(typeof val == 'object') 
    return [val]
  if(typeof val == 'undefined')
    return [] 
};

_n.object = function(input, key) {
  const o = {};
  return input.reduce((obj, item) => {
    return {
      ...obj,
      [item[key]]: item,
    };
  }, o);
}

_n.filter = (what, how) => {

  if(Array.isArray(what))
    return what.filter((key, val) => how(key, val))

  if(typeof what == 'object') 
    return _n.flow([
      Object.entries,
      arr => arr.filter( ([key, val]) => { return how(val, key) } ),
      Object.fromEntries
    ])(what);
}

_n.copy = function(input) {
  return JSON.parse(JSON.stringify(input));
};

// _n.index = function ( item, collection ) {
//   return [].slice.call(collection).indexOf(item);
// }

// array of uniqe values, filtered by self, or path to property that should be unique
_n.unique = (input, by) => {
  return input.filter((value, index, self)=>{
    if(!!by){
      return self.map(i=>i[by]).indexOf(value[by]) === index;
    } else {
      return self.indexOf(value) === index;
    }
  })
}

_n.flatten = array_of_arrays => {
  return array_of_arrays.reduce( (flat, toFlatten) => {
    return flat.concat(Array.isArray(toFlatten) ? _n.flatten(toFlatten) : toFlatten);
  }, []);
}

_n.set = array_of_arrays => {
  return _n.unique(_n.flatten(array_of_arrays))
}

_n.remove = (values, array) => {
  _n.array(values).forEach((value) => {
    array.splice(array.indexOf(value), 1);
  });
  return array;
};

// deep array and object data mapping
_n.map = (o, m) => {
  let is_array = Array.isArray(o);
  let mapped_data = _n.array(o).map((data) => {
    let mapped = {};
    for (let prop in m) {
      if(!!m[prop].value){
        if(typeof m[prop].value == 'string') {
          mapped[prop] = _n.literal(m[prop].value, data);
        } else {
          mapped[prop] = m[prop].value;
        }
      } else if(!!m[prop].from){
        mapped[prop] = _n.nested(data, m[prop].from);
        if(!!m[prop].each && Array.isArray(mapped[prop])){
          mapped[prop] = mapped[prop].map(i=>{
            return _n.map(i,m[prop].each)
          })
        }
      } else if (Array.isArray(m[prop])) {
        mapped[prop] = _n.map(data[m[prop][0]], m[prop][1]);
      } else if (m[prop].includes(".")) {
        try {
          mapped[prop] = m[prop].split("|")[0].split(".").reduce((ob, i) => ob[i], data);
        } catch (err) {}
      } else mapped[prop] = data[m[prop].split("|")[0]];

      // piped functions
      if(typeof m[prop] == 'string' && m[prop].includes("|")){
        m[prop].split("|").forEach( (pf,i) => {
          if ( i === 0 ) return;
          if (pf.includes('val')) return mapped[prop] = eval(pf.replace('val','mapped[prop]'))
          mapped[prop] = eval('mapped[prop]'+pf)
        })
      }
    }
    return mapped;
  });
  if (!is_array) return mapped_data[0];
  return mapped_data;
};

// Deep merge two objects.
_n.merge = (target, ...sources) => {
  if (!sources.length) return target;
  const source = sources.shift();

  if (_n.is_object(target) && _n.is_object(source)) {
    for (const key in source) {
      if (_n.is_object(source[key])) {
        if (!target[key]) Object.assign(target, { [key]: {} });
        _n.merge(target[key], source[key]);
      } else {
        Object.assign(target, { [key]: source[key] });
      }
    }
  }
  return _n.merge(target, ...sources);
}

_n.is_object = item => {
  return (item && typeof item === 'object' && !Array.isArray(item));
}
_n.is_array = item => {
  return (item && typeof item === 'object' && Array.isArray(item));
}

_n.nested = (obj, path) => {
  return path.split('.').reduce((o, i) => !!o[i] && o[i], obj);
}
_n.dot = (path, obj) => {
  return _n.nested(obj, path)
}
_n.literal = (template, data) => {
  return new Function(...Object.keys(data), `return \`${template}\`;`)(...Object.values(data))
}

_n.combine = ([head, ...[headTail, ...tailTail]]) => {
  
  if (!headTail) return _n.array(head).map(i=>{
    if(typeof i == 'array') i.reverse();
    return i;
  })

  const combined = headTail.reduce((acc, x) => {
    return acc.concat(head.map(h => _n.array(x).concat(h)))
  }, [])

  return _n.combine([combined, ...tailTail])
}

_n.sum = a => {
  return _n.array(a).reduce((a, b) => a + b, 0)
}

_n.sample = (arr, size) => {
  var shuffled = arr.slice(0), i = arr.length, min = i - size, temp, index;
  while (i-- > min) {
      index = Math.floor((i + 1) * Math.random());
      temp = shuffled[index];
      shuffled[index] = shuffled[i];
      shuffled[i] = temp;
  }
  return shuffled.slice(min);
}

_n.range = (min,max,step) => {

  step = step || 1

  let arr = [];
  let d = 0

  if ((step % 1) != 0) d = step.toString().split(".")[1].length;  
    
  for(let i = (min || 0); i <= (max || 99); i += (step || 1)){
    if(d){
      arr.push(i.toFixed(d))
    } else {
      arr.push(i)
    }
  }
  return arr;
}

_n.empty = thing => {
  if(!!thing.nodeName){
    if(thing.childElementCount == 0){
      return true
    }
    return false;
  }
  if(Array.isArray(thing) && thing.length == 0) return true;
  if(typeof thing == 'object' && Object.keys(thing).length == 0) return true;
  return false;
}

_n.height = (el) => {
  el = typeof el === "string" ? document.querySelector(el) : el;

  var styles = window.getComputedStyle(el);
  var margin =
    parseFloat(styles["marginTop"]) + parseFloat(styles["marginBottom"]);

  return Math.ceil(el.offsetHeight + margin);
};

_n.inview = (el, offset, config={}) => {
  if (el.offsetParent === null) return false;
  offset = offset || 0;
  var bounding = el.getBoundingClientRect();
  var elTop = bounding.top + offset;
  var elBottom = bounding.bottom - offset;
  return (
      elTop >= 0 &&
      elTop <= (window.innerHeight + offset) &&
      bounding.left >= 0 &&
      (elBottom <= (window.innerHeight || document.documentElement.clientHeight) || config.edge == 'top')&&
      bounding.right <= (window.innerWidth + 1  || document.documentElement.clientWidth + 1)
  );
};

_n.visible = (elem, config={}) => {
    if (!(elem instanceof Element)) throw Error('DomUtil: elem is not an element.');
    const style = getComputedStyle(elem);
    if (style.display === 'none') return false;
    if (style.visibility !== 'visible') return false;
    if (style.opacity < 0.1) return false;
    if (elem.offsetWidth + elem.offsetHeight + elem.getBoundingClientRect().height +
        elem.getBoundingClientRect().width === 0) {
        return false;
    }
    const elemCenter   = {
        x: elem.getBoundingClientRect().left + elem.offsetWidth / 2,
        y: elem.getBoundingClientRect().top + elem.offsetHeight / 2
    };
    if (elemCenter.x < 0) return false;
    if (elemCenter.x > (document.documentElement.clientWidth || window.innerWidth)) return false;
    if (elemCenter.y < 0) return false;
    if (elemCenter.y > (document.documentElement.clientHeight || window.innerHeight)) return false;
    let pointContainer = document.elementFromPoint(elemCenter.x, elemCenter.y);
    do {
        if (pointContainer === elem) return true;
    } while (pointContainer = pointContainer.parentNode);
    return false;
};

_n.docheight = () => {
  const body = document.body;
  const html = document.documentElement;

  return Math.max(
    body.scrollHeight,
    body.offsetHeight,
    html.clientHeight,
    html.scrollHeight,
    html.offsetHeight
  );
};

_n.random = a => {
  a = a || 15
  if (Number.isInteger(a))
    a = {length:a};
  let result = '';
  a.from = a.from || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for ( var i = 0; i < a.length; i++ ) {
    result += a.from.charAt(Math.floor(Math.random() * a.from.length));
  }
  return result;
};

_n.urlparams = {
  parse: ( url ) => {
    url = url || document.location.href
    let paramstrings = url
    if(url.includes('?')) paramstrings = url.split('?')[1]
    paramstrings = paramstrings.split('#')[0]
    const params = {}
    paramstrings.split('&').forEach((part)=>{
      const v = part.split('=')
      if(v[0]&&v[1])
        params[decodeURIComponent(v[0])] = decodeURIComponent(v[1])
    })
    return params
  },
  get: ( p ) => {
    let paramstrings = document.location.href.split('?') 
    if(paramstrings.length==1) {
      if(!!p) return false;
      return [];
    }
    paramstrings[1] = paramstrings[1].split('#')[0]
    const params = {}
    paramstrings[1].split('&').forEach((part)=>{
      const v = part.split('=')
      if(v[0]&&v[1])
        params[decodeURIComponent(v[0])] = decodeURIComponent(v[1])
    })
    if(!!p) {
      if(!!params[p]) 
        return params[p]; 
      else 
        return false;
    } 
    return params 
  },
  set: (k, v, e) => {
    const po = _n.urlparams.get();
    po[k] = v;
    return _n.urlparams.build(po, e);
  },
  join: (k, v, d, e) => {
    if (!e && typeof d == "boolean") {
      e = d;
      d = ",";
    }
    d = d || ",";
    const po = _n.urlparams.get();
    if (!po[k]) {
      po[k] = v;
    } else {
      const a = po[k].split(d);
      a.push(v);
      po[k] = a.join(d);
    }
    return _n.urlparams.build(po, e);
  },
  unjoin: (k, v, d, e) => {
    if (!e && typeof d == "boolean") {
      e = d;
      d = ",";
    }
    d = d || ",";
    const po = _n.urlparams.get();
    if (!k in po) {
      return false;
    } else {
      po[k] = po[k]
        .split(d)
        .filter((p) => p !== v)
        .join(d);
      if (!po[k]) delete po[k];
    }
    return _n.urlparams.build(po, e);
  },
  jointoggle: (k, v, d, e) => {
    if (!e && typeof d == "boolean") {
      e = d;
      d = ",";
    }
    d = d || ",";
    const po = _n.urlparams.get();
    if (!po[k] || !po[k].split(d).includes(v)) {
      return _n.urlparams.join(k, v, d, e);
    } else {
      return _n.urlparams.unjoin(k, v, d, e);
    }
  },
  toggle: (k, v, e) => {
    const po = _n.urlparams.get();
    if (po[k] == v) delete po[k];
    else po[k] = v;
    return _n.urlparams.build(po, e);
  },
  unset: (k, e) => {
    const po = _n.urlparams.get();
    delete po[k];
    return _n.urlparams.build(po, e);
  },
  build: (o, e) => {
    const ret = [];
    for (let d in o)
      if (e) ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(o[d]));
      else ret.push(d + "=" + encodeURIComponent(o[d]));
    return ret.join("&") + document.location.hash;
  },
};

_n.equal = (value, other) => {
  // Get the value type
  var type = Object.prototype.toString.call(value);

  // If the two objects are not the same type, return false
  if (type !== Object.prototype.toString.call(other)) return false;

  // If items are not an object or array, return false
  if (["[object Array]", "[object Object]"].indexOf(type) < 0) return false;

  // Compare the length of the length of the two items
  var valueLen =
    type === "[object Array]" ? value.length : Object.keys(value).length;
  var otherLen =
    type === "[object Array]" ? other.length : Object.keys(other).length;
  if (valueLen !== otherLen) return false;

  // Compare two items
  var compare = function(item1, item2) {
    // Get the object type
    var itemType = Object.prototype.toString.call(item1);

    // If an object or array, compare recursively
    if (["[object Array]", "[object Object]"].indexOf(itemType) >= 0) {
      if (!isEqual(item1, item2)) return false;
    }

    // Otherwise, do a simple comparison
    else {
      // If the two items are not the same type, return false
      if (itemType !== Object.prototype.toString.call(item2)) return false;

      // Else if it's a function, convert to a string and compare
      // Otherwise, just compare
      if (itemType === "[object Function]") {
        if (item1.toString() !== item2.toString()) return false;
      } else {
        if (item1 !== item2) return false;
      }
    }
  };

  // Compare properties
  if (type === "[object Array]") {
    for (var i = 0; i < valueLen; i++) {
      if (compare(value[i], other[i]) === false) return false;
    }
  } else {
    for (var key in value) {
      if (value.hasOwnProperty(key)) {
        if (compare(value[key], other[key]) === false) return false;
      }
    }
  }

  // If nothing failed, return true
  return true;
};

_n.poll = (callback, i, c) => {
  let intervalCount = 0
  let interval = i || 1000
  let count = c || 1
  const repeat = setInterval(() => {
    callback(repeat)
    intervalCount++
    if (intervalCount >= count) {
      clearInterval(repeat)
    }
  }, interval)
}

_n.fraction = (value, donly = true) => {
  var tolerance = 1.0E-6; // from how many decimals the number is rounded
  var h1 = 1;
  var h2 = 0;
  var k1 = 0;
  var k2 = 1;
  var negative = false;
  var i;

  if (parseInt(value) == value) { // if value is an integer, stop the script
    return value;
  } else if (value < 0) {
    negative = true;
    value = -value;
  }

  if (donly) {
    i = parseInt(value);
    value -= i;
  }

  var b = value;

  do {
    var a = Math.floor(b);
    var aux = h1;
    h1 = a * h1 + h2;
    h2 = aux;
    aux = k1;
    k1 = a * k1 + k2;
    k2 = aux;
    b = 1 / (b - a);
  } while (Math.abs(value - h1 / k1) > value * tolerance);

  return (negative ? "-" : '') + ((donly & (i != 0)) ? i + ' ' : '') + (h1 == 0 ? '' : h1 + "/" + k1);
}

_n.sort = (el, how, as, rev, order) => {

  let sorted = [...el.children]

  if(how !== 'manual') {
  
    sorted.sort((a, b) => {
      
      let type = !isNaN(eval("a." + how)) ? "numeric" : "alpha";
      if (!!as) type = as;

      if (type === "numeric") {
        if(!!eval("a." + how).match(/[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)/g) > 0 && !!eval("b." + how).match(/[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)/g) > 0){
          return (parseFloat(eval("a." + how).match(/[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)/g)[0]) - parseFloat((eval("b." + how).match(/[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)/g)[0])));  
        } else {
          return eval("a." + how) - eval("b." + how);
        }
        
      } else {
        return eval("a." + how) > eval("b." + how) ? 1 : -1;
      }

    });
  }

  if(!!order){
    const values = sorted.map(el=>eval("el." + how))
    order = order.map( item => {
      if(values.indexOf(item)!==-1) values.splice(values.indexOf(item), 1)
      return item;
    }).concat(values)
    sorted = sorted.sort( (a,b) => {
      return order.indexOf( eval('a.'+how) ) - order.indexOf( eval('b.'+how) );
    })
  }

  if (rev) sorted = sorted.reverse();

  sorted.forEach((node) => el.appendChild(node));
  
  return sorted;
};

_n.height = {}
_n.height.match = qs => {
  
  _n.qsa(qs).forEach(el=>{
    el.style.removeProperty('height')
  })
  
  _n.qsa(qs).forEach(el=>{
    var aligned = _n.qsa(qs).filter(o => o.getBoundingClientRect().top == el.getBoundingClientRect().top)
    el.style.height = `${Math.max(...aligned.map(a=>a.scrollHeight))}px`;
  })
}

_n.sortBy = () => {
  var props = arguments;
  return function (obj1, obj2) {
      var i = 0, result = 0, numberOfProperties = props.length;
      while(result === 0 && i < numberOfProperties) {
          result = _n.sortBy(props[i])(obj1, obj2);
          i++;
      }
      return result;
  }
}

// _n.debounce = (callback, wait) => {
//   let timeoutId = null;
//   return (...args) => {
//     window.clearTimeout(timeoutId);
//     timeoutId = window.setTimeout(() => {
//       callback.apply(null, args);
//     }, wait);
//   };
// }

_n.breakpoints = {sm:640,md:768,lg:1024,xl:1280,"2xl":1536};

_n.cal = (...args) => {
  const d = new Date(...args)
  const r = {
    obj: d,
    weekDays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  }
  r.year = d.getFullYear()
  r.month = {
    i: d.getMonth(),
  }

  r.month.name = r.months[r.month.i];
  r.month.number = (r.month.i + 1).toString().padStart(2,'0');
  r.month.total_days = new Date(r.year, r.month.i + 1, 0).getDate();
  r.month.weekday_offset = new Date(r.year, r.month.i, 1).getDay();
  r.month.days=_n.range(1,r.month.total_days).map(md=>{
    return md
    _n.date(`${d.getFullYear()}-${r.month.i+1}-${md}`)
  })
  r.day = d.getDay()
  r.date = d.getDate()
  r.month.name = r.months[r.month.i]
  return r;
}

String.prototype.handle = function() {
  var handle = this.replace(/%/g,'').replace("'", "").toLowerCase().replace(/[^\w\u00C0-\u024f]+/g, "-").replace(/^-+|-+$/g, "")
  try {
    handle = decodeURIComponent(this.replace(/%/g,'')).replace("'", "").toLowerCase().replace(/[^\w\u00C0-\u024f]+/g, "-").replace(/^-+|-+$/g, "")
  } catch(err){}
  return handle
}
String.prototype.capitalize = function() {
  str = this.toLowerCase();
  return str.replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g,
    function(s){
      return s.toUpperCase();
  });
};

_n.http = {
  // utility for fetch based HTTP error handling
  err: (response) => {
    if(!response.ok) {
      throw Error(response.statusText);
    }
    return response
  }
}

_n.stripHTML = input => {
  return input.replace(/(<([^>]+)>)/gi, "");
}

/* Dispatch Events */
_n.trigger = (el=window, eventName, bubbles=true, cancelable=true, details = false) => {
  if (el === undefined || eventName === undefined) return false;
  var event = new CustomEvent(eventName, {
    detail: {
      message: el,
      time: new Date(),
      info: details,
    },
    bubbles: bubbles,
    cancelable: cancelable,
  });
  return el.dispatchEvent(event);
};

_n.throttle = (func, wait=100, immediate) => {
  let inThrottle
  return function() {
    const args = arguments
    const context = this
    if (!inThrottle) {
      func.apply(context, args)
      inThrottle = true
      setTimeout(() => inThrottle = false, wait)
    }
  }
}
_n.debounce = (func, wait=100, immediate) => {
  var timeout
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  }
}
_n.wait = (ms) => {
  return new Promise( (res, rej) => {
    setTimeout(()=>{
      res(true)
    },ms)
  })
}

window.addEventListener("resize",_n.debounce(e=>{
  _n.trigger(window, 'resized')
}));


window._n = _n;

export default _n;
