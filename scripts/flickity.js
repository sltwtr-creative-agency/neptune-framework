import Neptune from './neptune'
import Flickity from 'flickity'
import FlickityImagesLoaded from 'flickity-imagesloaded';
import FlickityAsNavFor from 'flickity-as-nav-for';

Neptune.flickity = {
  init: (p,force) => {
    p = p || document

    if(!!force)
      Neptune.flickity.destroy(p)

    _n.qsa('[data-flickity]:not(.flickity-enabled), [neptune-flickity]:not(.flickity-enabled), [data-simple-carousel]:not(.flickity-enabled), [data-simple-carousel-nav]:not(.flickity-enabled)', p).forEach( f => {
      Neptune.flickity.process(f)
    })
  },
  destroy: p => {
    p = p || document
    _n.qsa('[neptune-flickity].flickity-enabled', p).forEach( f => {
      f.flkty.destroy()
    })
  },
  resize: p => {
    p = p || document
    _n.qsa('[neptune-flickity].flickity-enabled', p).forEach( f => {
      f.flkty.resize()
    })
  },
  process: f => {
    if(!!f.getAttribute('neptune-flickity')){
      f.options = USON.parse(f.getAttribute('neptune-flickity'))[0]
    } else {
      f.options = Object.assign({}, f.dataset);
    }
    for(let key in f.options){
      if (f.options[key] == "true") f.options[key] = true;
      if (f.options[key] == "false") f.options[key] = false;
      if ([key] == "asNavFor") f.options[key] = _n.qs(f.options[key]);
      if (window.outerWidth < 768 && key.includes('Mobile') ) {
        f.options[key.replace('Mobile','')] = f.options[key]
        delete f.options[key]
      }
    }
    if( 
      !!f.previousElementSibling && 
      ( f.previousElementSibling.hasAttribute('neptune-flickity') || f.previousElementSibling.hasAttribute('data-simple-carousel') )
    ){
      // const simmer = new Simmer(window, {})
      f.options.asNavFor = _n.rqs(f.previousElementSibling);
    }

    f.flkty = new Flickity( f, f.options);
  }
};

window.Neptune = Neptune

export default Neptune.flickity;