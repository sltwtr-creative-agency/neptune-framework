import serialize from '../../node_modules/form-serialize'

Neptune.cart = {}

Neptune.cart.onCartUpdate = (cart) => {
  if(!cart.token) return false;
  window.cart = cart
  Neptune.cart.decode()
  _n.trigger(document,'cart:updated',false,true,cart)
}

Neptune.cart.addItem = (id,quantity,properties,callback) => {
  Neptune.cart.add({
    id:id,
    quantity:quantity,
    properties:properties
  }, callback)
}


Neptune.cart.add = (items, callback) => {
  items = _n.array(items).map(item=>{
    const i = {
      id: item.id || item,
      quantity: item.quantity || 1,
      properties: item.properties || {}
    }
    if (!!i.properties['selling_plan']){
      i.selling_plan = i.properties['selling_plan']
      delete i.properties['selling_plan']
    }
    return i
  })
  items = items.filter( item => {
    return !! item.id 
  });
  if(!items.length) return false;
  _n.trigger(document,'cart:itemAdding',false,true)
  return fetch(window.Shopify.routes.root+'cart/add.js', {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({items:_n.array(items)})
  })
  .then(r => r.json())
  .then(d => {
    if(!!d.status){
      window.cart.error = d
      _n.trigger(document,'cart:error',false,true,d)
    } else {
      if ((typeof callback) === 'function') {
        callback(d)
      }
      Neptune.cart.getCart()
      d.items_added = items
      _n.trigger(document,'cart:itemAdded',false,true,d)
    }
    
    // Neptune.cart.onCartUpdate(cart)
  });
}

Neptune.cart.update = (updates, callback) => {

  if(_n.empty(updates)) return false
  
  return fetch(window.Shopify.routes.root+'cart/update.js', {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      updates: updates
    })
  })
  .then(r => {
    if ((typeof callback) === 'function') {
      callback(r.json())
    }
    Neptune.cart.onCartUpdate(r.json())
  })
}

Neptune.cart.change = (change, callback) => {

  if(_n.empty(change)) return false
  
  return fetch(window.Shopify.routes.root+'cart/change.js', {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(change)
  })
  .then(r => {
    if ((typeof callback) === 'function') {
      callback(r.json())
    }
    Neptune.cart.getCart()
  })
}

Neptune.cart.remove = (remove, callback) => {
  if (typeof remove == 'number' && remove < 1000) {
    // it's an index
    if(!cart.items[remove]) return;
     var u = {
      line:remove+1,
      quantity:0
     }
    // console.log('remove by change',u)
    _n.trigger(document,'cart:itemRemoved',false,true,cart.items[remove])
    return Neptune.cart.change(u, callback)
  } else {
    // it's a variant id or array
    var u = {}
    _n.array(remove).forEach(v=>{
      u[v]=0
    })
    if(!Object.keys(u).length) return
    _n.trigger(document,'cart:itemRemoved',false,true,cart.items.find(i=>i.id==remove))
    return Neptune.cart.update(u, callback)
  }
  return false
}


Neptune.cart.addItemFromForm = (form, callback, errorCallback) => {
  form = serialize(form, {hash: true})
  fetch(window.Shopify.routes.root+'cart/add.js', {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(form)
  })
    .then(r => {
      if ((typeof callback) === 'function') {
        callback(r.json())
      } else {
        Neptune.cart.onCartUpdate(r.json())
      }
    })
}

Neptune.cart.getCart = (callback) => {
  return fetch(window.Shopify.routes.root+'cart.js', { credentials: 'include' })
    .then(r => r.json())
    .then(cart => {

      if ((typeof callback) === 'function') {
        callback(cart)
      } else {
        Neptune.cart.onCartUpdate(cart)
      }
    })
}

Neptune.cart.decode = ()=>{
  Object.keys(cart.attributes).forEach(att=>{
    if (typeof cart.attributes[att] == 'string' && cart.attributes[att].includes('{')) 
      cart.attributes[att] = JSON.parse(cart.attributes[att].replace(/=>/g,':'));
  })
}

Neptune.cart.changeItem = (line, quantity, properties, callback) => {

  console.log('Neptune.cart.changeItem')
  
  let data = { line: line+1, quantity: quantity }
  if (!!properties) data.properties = properties

  fetch(window.Shopify.routes.root+'cart/change.js', {
    method: 'POST',
    credentials: 'same-origin',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(r => r.json())
  .then(response => {
    if(response.status == 422){
      let errorMessage = response.description
      if (typeof cartMessages !== 'undefined' && cartMessages.quantityError) {
        errorMessage = cartMessages.quantityError.replace('[quantity]', cart.items[line].quantity).replace('[name]', cart.items[line].title)
      } 
      cart.items[line].error = errorMessage
      _n.trigger(document,'cart:error',false,true,response)
      Neptune.cart.onCartUpdate(cart)
    } else {
      if(!!callback){
        callback(response)
      } else {
        Neptune.cart.onCartUpdate(response)
      }
    }
  })
}


Neptune.cart.attributes = (attributes, callback) => {
  let data = { attributes: attributes }
  return new Promise(res=>{
    fetch(window.Shopify.routes.root+'cart/update.js', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(r => r.json())
    .then(cart => {
      // console.log('attribute update response', cart)
      if(cart.status == 422){
        _n.qs('[neptune-message="cart"]').innerHTML = cart.description
      } else {
        if(!!callback){
          callback(cart)
        } else {
          Neptune.cart.onCartUpdate(cart)
          res(true)
        }
      }
    })
  })
}

Neptune.cart.note = note => {
  fetch(window.Shopify.routes.root+'cart/update.js', {
    method: 'POST',
    credentials: 'same-origin',
    body: JSON.stringify({note:note}),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(r => r.json())
  .then(cart => {
    // console.log(cart)
  })
}

if(window.cart) Neptune.cart.decode();

window.addEventListener("pageshow", event => {
  if (event.persisted){
    Neptune.cart.getCart()
  }
});

window.Neptune = Neptune

export default Neptune.cart
