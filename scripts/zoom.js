Neptune.zoom = {
  init: p => {
    
    p = p || document;
    
    _n.qsa('[neptune-zoom]', p).forEach( el => {

      el.zoom = USON.parse(el.getAttribute('neptune-zoom'))[0];
      
      if (!!el.zoom.on) {
        _n.array(el.zoom.on).forEach( evt => {
          el.addEventListener(evt, e => {
            Neptune.zoom.on(e.target)
          })
        })
      }
      if(!!el.zoom.off){
        _n.array(el.zoom.off).forEach( evt => {
          el.addEventListener(evt, e=>{Neptune.zoom.off(e.target)} )
        })
      }
      
      if(!!el.zoom.toggle){
        _n.array(el.zoom.toggle).forEach( evt => {
          el.addEventListener(evt, e => { 
            eptune.zoom.toggle(e.target) ;
          })
        })
      }

      el.addEventListener('mousemove', e => {
        let offsetX = 50
        let offsetY = 50
        e.offsetX ? offsetX = e.offsetX : 50
        e.offsetY ? offsetY = e.offsetY : 50
        let x = offsetX / el.offsetWidth * 100
        let y = offsetY / el.offsetHeight * 100
        el.style.backgroundPosition = x + '% ' + y + '%'
      })

    })
  },
  on: el => {
    el.classList.add('zoomable-active')
  },
  off: el => {
    el.classList.remove('zoomable-active')
  },
  toggle: el => {
    el.classList.toggle('zoomable-active')
  }
}

window.Neptune = Neptune

export default Neptune.zoom;