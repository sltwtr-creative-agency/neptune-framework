import Neptune from'./neptune';
import _n from './_n';

Neptune.slider = {
  init: () => {


_n.qsa('[data-slider], [neptune-slider]:not(.slider-initialized)').forEach(function(c){

  c.classList.add('slider-initialized');
  
  const scroller = _n.qs('.flex-slider',c)
  const slides = _n.qsa('[data-slide]',c)
  const itemSize = _n.qs('[data-slide]',scroller).clientWidth
  const snapAlign = ((scroller.hasAttribute('data-align')) ? scroller.getAttribute('data-align') : 'center');
  const toClicked = ((c.hasAttribute('data-toclicked')) ? true : false);
  const toFocused = ((c.hasAttribute('data-tofocused')) ? true : false);
  const transferFocus = ((c.hasAttribute('data-transferfocus')) ? true : false);
  const activeThreshold =  ((scroller.hasAttribute('data-threshold')) ? Number(scroller.getAttribute('data-threshold')) : 5);
  
  if(_n.qsa('[data-slide-count]',c).length)
    _n.qs('[data-slide-count]',c).innerHTML = slides.length
    
  c.activeSlide = _n.qsa('[data-slide]',scroller)[0]
  c.activeSlideIndex = 0

  c.next = function () {
    scroller.scrollBy({
      left: itemSize,
      behavior: 'smooth'
    });
  }
  c.prev = function () {
    scroller.scrollBy({
      left: -itemSize,
      behavior: 'smooth'
    });
    _n.trigger(scroller,'touchMove')
  }
  c.to = function (index) {
    if ('scrollBehavior' in scroller.style || !smoothScroll) {
    scroller.scrollTo({
      left: itemSize * index,
      behavior: 'smooth'
    });
    c.activeSlide =  _n.qsa('[data-slide]',c)[index]
    c.activeSlideIndex = index
    
    } else {
      smoothScrollTo(scroller, 0, itemSize * index, 500);
    }
  }
  c.paginate = function(e) {
    // var nodes = Array.prototype.slice.call( e.target.parentElement.children );
    // console.log(nodes)
    // var index = _n.index(e.target.parentNode.parentNode,e.target);
    // alert(index)
    // c.to(index)
  }
  
  if(_n.exists('[data-prev]',c)) _n.qs('[data-prev]',c).onclick = c.prev
  if(_n.exists('[data-next]',c)) _n.qs('[data-next]',c).onclick = c.next

  _n.qsa('[data-slider-pagination] > *',c).forEach(function(d,i){
      d.addEventListener('click',function(){c.to(i)})
  })
  if(toClicked){
    _n.qsa('[data-slide]',c).forEach(function(d,i){
      d.addEventListener('click',function(){
        c.to(i)
      })
    })
  }
  if(toFocused){
    _n.qsa('[data-slide]',c).forEach(function(d,i){
      d.addEventListener('focus',function(){
        c.to(i)
      })
    })
  }
  c.isScrolling = false
  c.scrollStopper = {}
  c.activeSlide = 0
  
  c.scrolls = function (e) {
    window.clearTimeout( c.scrollStopper )
    if(!c.isScrolling){
      c.isScrolling = true;
      c.setAttribute('data-scrolling',true)
      // c.unpage()
    }
    c.scrollStopper = setTimeout(c.stopped,123)
  }
  scroller.addEventListener('scroll', c.scrolls);
  
  c.stopped = function() {
    c.setAttribute('data-scrolling',false)
    c.isScrolling = false;
    c.page()
  }
  
  c.unpage = function() {
    c.activeSlideIndex = false
    c.activeSlide = false
    _n.qsa('[data-slider-pagination] > *',c).forEach(function(d,i){
     d.classList.remove('active')
     if(d.matches('input')) d.checked=false
    })
    if(_n.qsa('[data-active-slide]',c).length)
      _n.qs('[data-active-slide]',c).removeAttribute('data-active-slide')
  }
  
  c.page = function() {
   
    c.unpage()
   
    _n.qsa('[data-slide]',scroller).forEach(function(slide,index){
      slide.setAttribute('aria-hidden',true)
    })
    _n.qsa('[data-slide]',scroller).forEach(function(slide,index){

      // there can be only one
      if(c.activeSlide) return

      let isActiveSlide = false

      if(index===0 && scroller.scrollLeft==0) isActiveSlide = true;
      if(index===0 && scroller.scrollLeft>0) return;

      let scrollPos
      let slidePos

      if(snapAlign=='start') {
       scrollPos = scroller.scrollLeft
       slidePos =  slide.offsetLeft
      }

      if(snapAlign=='end') {
       scrollPos = scroller.scrollLeft+scroller.clientWidth
       slidePos = slide.offsetLeft+slide.clientWidth
      }

      if(snapAlign=='center') {
       scrollPos = scroller.scrollLeft+(scroller.clientWidth/2)
       slidePos = slide.offsetLeft+(slide.clientWidth/2)
      }

      if(index===0) {
       scrollPos = 0
       slidePos = 0
      }

      if(index==_n.qsa('[data-slide]',scroller).length-1) {
       scrollPos = scroller.scrollWidth
       slidePos = scroller.scrollLeft+scroller.clientWidth
      }

      if(scrollPos > slidePos-activeThreshold && scrollPos < slidePos+activeThreshold)
       isActiveSlide = true;
        
      if(isActiveSlide){
      c.activeSlideIndex = index
      c.activeSlide = slide
      c.setAttribute('data-active-slide-index',index)
      slide.setAttribute('data-active-slide',true)
      slide.removeAttribute('aria-hidden')
       
       if(transferFocus){
         setTimeout( () => {
          slide.focus()
         },66)
       }
      }
    }) //each slide
   
    _n.qsa('[data-slider-pagination] > *',c).forEach(function(d,i){
      if(i==c.activeSlideIndex) d.classList.add('active')
    })

    _n.qsa('[data-slider-pagination] > *',c).forEach(function(d,i){
      if(i==c.activeSlideIndex) d.classList.add('active')
     
      if(d.matches('input')) d.checked=false

      if(i==c.activeSlideIndex && d.matches('input')) d.checked=true
      
    });
    
    // if(_n.qsa('[data-slide-count]',c).length) _n.qs('[data-slide-count]',c).innerHTML = _n.qsa('[data-slide]',c).length
    
    if(_n.qsa('[data-current]',c).length) _n.qs('[data-current]',c).innerHTML = c.activeSlideIndex + 1
    
    _n.trigger(c,'slider:changed',false,true,c)
    _n.trigger(document,'slider:changed',false,true,c)
    
    return c.activeSlideIndex;
   
  }
  _n.trigger(c,'slider:ready',false,true,c)
  c.page()

});

}}

window.Neptune = Neptune

export default Neptune.slider;
