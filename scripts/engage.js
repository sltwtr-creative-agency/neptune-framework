import Neptune from "./neptune";
import _n from "./_n";
import USON from "../../node_modules/uson";

window.USON = USON;
// USON.parseOriginal = USON.parse;
// USON.parse = (a,b) => {
//   try {
//     USON.parseOriginal(a,b)
//   } catch(err) {
//     console.log('USON Parse Error', err, a)
//   }
// }
// test

/**
 * engage
 * @SLTWTR
 * A lightweight, versitile tool to add interactivity to a web property informed by data attributes on trigger or target elements
 * Utilized by placing JSON in
 * Dependencies: _, _n.json, _n.array, _n.qs, _n.qsa, _n.copy, uson
 */

Neptune.engage = {
  default: {
    event: "click",
    trigger: "_self",
    targets: [
      {
        selector: "_self",
        classes: {
          toggle: "active",
        },
        attributes: [
          {
            att: "data-engaged",
            toggle: ["true", "false"],
          },
        ],
        focus: false,
        blur: false,
      },
    ],
  },

  init: function(p) {
    p = p || document;

    Neptune.debug.info("engage:init", { el: p });

    _n.qsa("[neptune-engage]", p).forEach(function(el) {

      if(el.getAttribute('permanent') && el.getAttribute('neptune-permanent')) return false;
      if(!!_n.parents(el,'[permanent]').nodeName) return false;
      
      let _engagements = [{}];

      try {
        _engagements = _n.array(
          USON.parse(
            el
              .getAttribute("neptune-engage")
              .replace(/'/g, '"')
              .replace(/`/g, "'"),
            "array"
          )[0]
        );
      } catch (error) {
        console.log('USON Parse Error', el, el.getAttribute(`neptune-engage`), error) 
      }

      if (JSON.stringify(_engagements) == "[{}]") {
        try {
          _engagements = _n.array(
            JSON.parse(
              el
                .getAttribute("neptune-engage")
                .replace(/'/g, '"')
                .replace(/`/g, "'"),
              "object"
            )
          );
        } catch (error) {} 
      }
      if (el.getAttribute("neptune-engage") != "" && !_engagements)
        Neptune.debug.error(
          "engage:parse",
          { el: el },
          el.getAttribute("neptune-engage")
        );
      else Neptune.debug.info("engage:parse", { el: el, data: _engagements });

      _engagements.forEach(function(_eng, ei) {

        if(!!_eng.breakpoints) {
          for( b in _eng.breakpoints) {
            // console.log(b)
          }
        }

        if (!!_eng.on) _eng.event = _eng.on;

        var _defaults = _n.copy(Neptune.engage.default),
          _engagement = Object.assign(_n.copy(Neptune.engage.default), _eng),
          trigger = el;

        if (!!el.getAttribute("data-engage-trigger"))
          target_el = _n.qs(_target.selector);

        if (
          _n.exists(_engagement.trigger) &&
          _engagement.trigger != "_self" &&
          !!_engagement.trigger
        )
          trigger = _n.qs(_engagement.trigger);

        if (
          !!_engagement.trigger &&
          _engagement.trigger.indexOf("_self") == 0 &&
          _engagement.trigger != "_self"
        )
          trigger = _n.qs(_engagement.trigger.replace("_self ", ""), el);

        if (!!!_engagement.event) _engagement.event = "click";

        _n.array(_engagement.event).forEach((event, _ei) => {

          if(event=='unclick') {
            trigger = document;
            event = 'click'
            var unclick = true
          }

          trigger.addEventListener(event, (e) => {
            if (trigger.engaging && !unclick) return false
            trigger.engaging = true
            
            Neptune.debug.info("engage:trigger", { el: trigger, event: e });

            if (
              !!e.which &&
              !!_engagement.which &&
              !_n.array(_engagement.which).includes(e.which)
            )
              return false;        

            if( unclick ) {
              if(el.contains(e.target) || el == e.target) {
                return false;
              } else {
              }
              // if(_n.parents(e.target,'[neptune-engage*="unclick"]') == el) {
              //   return false;
              // } else {
              // }
              // && (el.contains(e.target) || el == e.target) ) return false;
            }

            if (!!_engagement.preventDefault) e.preventDefault();

            if (!!_engagement.stopPropagation) e.stopPropagation();

            // ADD KEYS KEY VALUE FOR KEYUP EVENTS
            if (
              _engagement.keys &&
              _engagement.keys.includes(e.keyCode) === false
            )
              return false;

            // shorthand for targets
            // if(!_engagement.targets){
            //   for(let key in _engagement){
            //     if(_n.exists(key)){
            //       if(!_engagement.targets)
            //         _engagement.targets = []
            //       var target = _engagement[key]
            //       target.selector = key
            //       _engagement.targets.push(target)
            //     }
            //   }
            // }

            // console.log(_engagement)

            // set self as target if finds classes, attributes, siblings, functions or methods
            if(!_engagement.targets &&
              (
                !!_engagement.classes || 
                !!_engagement.attributes || 
                !!_engagement.siblings || 
                !!_engagement.functions || 
                !!_engagement.methods
              )
            ) {
              _engagement.targets = [_n.copy(_engagement)]
            }

            _engagement.targets = _engagement.targets || []
            for(let prop in _engagement) {
              if(_n.exists(prop)) {
                let t = _n.copy(_engagement[prop])
                t.selector = prop
                _engagement.targets.push(t)
              }
            }

            _n.array(_engagement.targets).forEach((_target, _ti) => {
              let delay = _target.delay || 0;

              setTimeout(function() {
                var target_els = [el];

                if (!!el.getAttribute("data-engage-target"))
                  target_els = _n.qsa(el.getAttribute("data-engage-target"));

                if (
                  !!_target.selector &&
                  _target.selector != "_self" &&
                  _target.selector != "_parent"
                )
                  target_els = _n.qsa(_target.selector);

                if (
                  !!_target.selector &&
                  _target.selector.indexOf("_self") == 0 &&
                  _target.selector != "_self"
                )
                  target_els = _n.qsa(
                    _target.selector.replace("_self ", ""),
                    el
                  );

                if (_target.selector && _target.selector == "_parent")
                  target_els = [el.parentNode];

                if (
                  !!_target.selector &&
                  _target.selector.indexOf("_parent") == 0 &&
                  _target.selector != "_parent"
                )
                  target_els = _n.qsa(
                    _target.selector.replace("_parent ", ""),
                    el.parentNode
                  );

                if (_target.selector && _target.selector == "_grandparent" && !!el.parentNode && !!el.parentNode.parentNode)
                  target_els = [el.parentNode.parentNode];

                if (
                  !!_target.selector &&
                  _target.selector.indexOf("_grandparent") == 0 &&
                  _target.selector != "_grandparent"
                )
                  target_els = _n.qsa(
                    _target.selector.replace("_grandparent ", ""),
                    el.parentNode.parentNode
                  );

                if(!!_target.parents)
                  target_els = [_n.parents(el, _target.parents)];

                if(!!_target.cousins && !!_target.parent ){
                  target_els = _n.cousins(el, _target.parent, _target.cousins);
                }

                target_els.forEach(function(target_el) {
                  Neptune.engage.action(target_el, _target, _engagement);
                });

                _n.trigger(document, "engage:after", false, true, _engagement);
              }, delay);
            });

            if (!!_engagement.history && _ei===0) {
              if (!!_engagement.history.push) {
                _engagement.event = "engage:popstate";
                let pushUrl =
                  _engagement.history.push.url || document.location.href;
                let pushTitle =
                  _engagement.history.push.title || _n.qs("title").innerText;
                let state = _engagement
                if (!!window.Turbolinks){
                  state.turbolinks=true
                  state.url=pushUrl
                }
                window.history.pushState(state, pushTitle, pushUrl);
              }

              if (!!_engagement.history.replace) {
                _engagement.event = "engage:popstate";
                let replaceUrl =
                  _engagement.history.replace.url || document.location.href;
                let replaceTitle =
                  _engagement.history.replace.title || _n.qs("title").innerText;
                let state = _engagement
                if (!!window.Turbolinks){
                  state.turbolinks=true
                  state.url=pushUrl
                }
                window.history.replaceState(state, replaceTitle, replaceUrl);
                window.history.replaceState(
                  state,
                  replaceTitle,
                  replaceUrl
                );
              }
            }

            setTimeout(()=>{delete trigger.engaging},50) // DEBOUNCE
            
          });
          if (event == "start") {
            _n.trigger(trigger, event, false, true);
          }
        });

        try{
          _engagement.targets.forEach(t=>{
            if (!!t.periscope && !!t.periscope.preload){
              Neptune.periscope.preload(t.periscope.view.url)
            }
          })
        } catch(err){}

      });
    });
  },

  action: function(target_el,_target,_engagement) {
    
    _target.classes = _target.classes || _target['C'] || {}

    //{body:-:[data-active-modal]}
    //{body:-:.modal-open,[data-active-modal]}
    //{body:+:.modal-open,[data-active-modal='value']}
    //{body:/:.modal-open}

    if (!target_el || !target_el.classList) return false;

    _n.trigger(document, "engage:before", false, true, _engagement);
    _n.trigger(target_el, "engage:before", false, true, _engagement);

    _target.classes.remove = _n.array(_target.classes.remove) || [];
    _target.classes.remove.forEach(function(cl) {
      target_el.classList.remove(cl);
    });

    _target.classes.add = _n.array(_target.classes.add) || [];
    _target.classes.add.forEach(function(cl) {
      target_el.classList.add(cl);
    });

    _target.classes.toggle = _n.array(_target.classes.toggle) || [];
    _target.classes.toggle.forEach(function(cl) {
      cl.split(" ").forEach(function(c) {
        target_el.classList.toggle(c);
      });
    });

    _target.attributes = _n.array(_target.attributes) || [];
    _target.attributes.forEach(function(a) {
      let toset;

      if (!!a.set) toset = a.set;

      if (!!a.toggle) {
        // console.log('toggle',a.toggle)
        a.toggle = _n.array(a.toggle);
        if (a.toggle.length == 1) a.toggle.push("_remove");
        toset = a.toggle[0];
        if (target_el.getAttribute(a.att) == a.toggle[0]) toset = a.toggle[1];
      }

      if (!!a.boolean) {
        toset = "true";
        if (target_el.getAttribute(a.att) == "true") toset = "false";
      }

      if (toset == "_false") {
        toset = "false";
      }

      if (toset == "_remove") {
        target_el.removeAttribute(a.att);
      } else {
        target_el.setAttribute(a.att, toset);
      }
    });

    if (!!_target.functions) {
      _target.functions.forEach(function(f) {
        let func = target_el[f].bind(target_el);
        func();
      });
    }

    if (!!_target.methods) {
      _n.array(_target.methods).forEach(function(f) {
        let func = target_el[f].bind(target_el);
        func();
      });
    }

    if (!!_target.html) {
      target_el.innerHTML = _target.html;
    }

    setTimeout(function() {
      if (!!_target.focus) {
        if (target_el.tabIndex != 0) target_el.tabIndex = 0;
          target_el.focus();
      }
      if (!!_target.blur) target_el.blur();
    }, 100);

    _n.trigger(target_el, "engage:action", false, true, _target);
    _n.trigger(document, "engage:action", false, true, _target);
    Neptune.debug.info("engage:action", { el: target_el, action: _target });

    if (!!_target.siblings) {
      _n.siblings(target_el).forEach((sibling) => {
        Neptune.engage.action(sibling, _target.siblings, _engagement);
      });
    }

    if (!!_target.children) {
      target_el.children.forEach((child) => {
        Neptune.engage.action(child, _target.children, _engagement);
      });
    }

    if (!!_target["periscope:view"] && !!Neptune.periscope) {
      Neptune.periscope.view(target_el, _target["periscope:view"].url);
    }

    if (
      !!_target.periscope &&
      !!_target.periscope.view &&
      !!Neptune.periscope
    ) {
      
      // console.log(_target);

      Neptune.periscope.view(
        target_el, 
        _target.periscope.view.url,
        {},
        false,
        !!_target.periscope.force);
    }

    if (!!_target["templates:load"] && !!Neptune.templates) {
      if (Array.isArray(_target["templates:load"]))
        Neptune.templates.load(
          _target["templates:load"][0],
          _target["templates:load"][1]
        );
      if (!!_target["templates:load"].data)
        Neptune.templates.load(
          _target["templates:load"].topic,
          _target["templates:load"].data
        );
    }

    if (!!_target["liquid:load"] && !!Neptune.liquid) {
      if (Array.isArray(_target["liquid:load"]))
        Neptune.liquid.load(
          _target["liquid:load"][0],
          _target["liquid:load"][1]
        );
      if (!!_target["liquid:load"].data)
        Neptune.liquid.load(
          _target["liquid:load"].topic,
          _target["liquid:load"].data
        );
    }
  }
};

window.addEventListener("popstate", (event) => {
  if (
    !!event.state &&
    !!event.state.event &&
    event.state.event == "engage:popstate"
  ) {
    Neptune.debug.info("engage:popstate", { el: window, actions: event.state });
    event.state.targets.forEach((_target) => {
      if (_n.exists(_target.selector))
        Neptune.engage.action(_n.qs(_target.selector), _target);
    });
  }
});

window.Neptune = Neptune;

export default Neptune.engage;

// (function(){
//   document.addEventListener('engage:before', function(e){
//     console.log(e)
//   })
//   document.addEventListener('engage:after', function(e){
//     console.log(e)
//   })
// })()
