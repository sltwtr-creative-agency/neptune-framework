const Sections = {

  init: () => {
    document.querySelectorAll('[id*="shopify-section-"]').forEach(section=>{
      window.sections.push({
        id:section.id.replace('shopify-section-',''),
        type:section.className.includes('section--') ? section.className.split('section--').at(-1).split(' ')[0] : section.id.replace('shopify-section-','').split('__').at(-1),
        //type:(Array.from(section.classList).find(c=>c.includes('shopify-section--'))||'').replace('shopify-section--',''),
        el:section,
        async:section.querySelectorAll('script[type="async/section"]').length>0,
        watch:true,
        view:{},
        data:{}
      })
    })
    Sections.watch()
  },

  watch: () => {

    sections = sections.map(section=>{

      const view = {
        position: section.el.getBoundingClientRect()
      }
      if (!!window.scrollDirection && window.scrollDirection == 'down') {
        view.approach = (view.position.top < (window.innerHeight + 500) && view.position.bottom >= 0)
      }
      if (!!window.scrollDirection && window.scrollDirection == 'up') {
        view.approach = view.position.bottom < 0 && view.position.bottom > -500
      }
      view.partial = (view.position.top < (window.innerHeight) && view.position.bottom >= 0)
      view.total = (view.position.top >= 0 && view.position.bottom <= window.innerHeight)
      view.fill = (view.position.top <= 0 && view.position.bottom >= window.innerHeight)
      view.exit = !view.partial

      if (view.position.top >= 0 && view.position.bottom <= window.innerHeight) {
          // The entire element is visible
          view.visibleHeight = view.position.height;
      } else if (view.position.top < 0 && view.position.bottom > window.innerHeight) {
          // The element is larger than the viewport and spans above and below it
          view.visibleHeight = window.innerHeight;
      } else if (view.position.top < 0 && view.position.bottom <= window.innerHeight) {
          // The top part of the element is out of view
          view.visibleHeight = view.position.bottom;
      } else if (view.position.top >= 0 && view.position.bottom > window.innerHeight) {
          // The bottom part of the element is out of view
          view.visibleHeight = window.innerHeight - view.position.top;
      } else {
          // The element is not visible in the viewport
          view.visibleHeight = 0;
      }
      section.el.style.setProperty('--percentage-viewport',`${Math.round((view.visibleHeight / window.innerHeight) * 100)}%`)

      // Observe Section state changes
      if(section.watch){
        ['approach','partial','total','exit'].forEach(state =>{
          if(section.view[state] !== view[state]) {
            
            _n.trigger(section.el,`Section:${state}`,true,true,{section:section,view:view})

            if(
              section.async && 
              !section.asyncLoaded && 
              state == 'approach' && 
              view[state]){

                Sections.fetch(section.id)
                section.asyncLoaded = true
            
            }

          }
        })
      }

      if(!view.approach && section.view.approach && view.partial && !section.view.partial) {
        _n.trigger(section.el,`Section:enter`,true,true,{section:section,view:view})
      }

      section.view = Object.assign(section.view, view);


      return section

    })

    var active = sections.map(section=>section.view.visibleHeight).reduce((maxIndex, currentValue, currentIndex, array) => currentValue > array[maxIndex] ? currentIndex : maxIndex, 0); 
    if((!Sections.active || Sections.active != active)&&!!sections[active]){ 
      Sections.active = active
      _n.trigger(sections[active].el,`Section:main`,true,true,sections[active])
    }

  },

  fetch: (ids, url='') => {

    if(ids==='*'){
      ids = sections.map(s=>s.id)
    } else if (ids.includes('*')) {
      ids = sections.map(s=>s.id).filter(id=>id.includes(ids.replace('*','')))
    } else if (ids.includes('!')) {
      ids = sections.map(s=>s.id).filter(id=>!id.includes(ids.replace('*','')))
    }

    fetch(`${url}?sections=${_n.array(ids).join(',')}`).then(r=>r.json()).then(template=>{
      _n.array(ids).forEach(id=>{
        document.querySelector(`#shopify-section-${id}`).innerHTML = template[id]
      })
    })
  }
  
}

window.sections = window.sections || []
window.Sections = Sections
Sections.init()
window.addEventListener('scroll',_n.throttle(e=>{Sections.watch()},500))
window.addEventListener('scroll',_n.debounce(e=>{Sections.watch()},500))