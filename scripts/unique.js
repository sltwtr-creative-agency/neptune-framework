import Neptune from "./neptune";
import _n from "./_n";

Neptune.unique = {
  init: function(p) {
    p = p || document;

    Neptune.debug.info("unique:init", { el: p });

    _n.qsa("[neptune-unique]", p).forEach((el) => {
      
      let _unique = {};

      try {
        if (!!el.getAttribute("neptune-unique")) {
          _unique = USON.parse(
            el
              .getAttribute("neptune-unique")
              .replace(/'/g, '"')
              .replace(/`/g, "'")
              .replace(/</g, "[")
              .replace(/>/g, "]"),
            "array"
          )[0];
        }
      } catch (error) {}

      let prop = 'innerText' 
      let children = [...el.children]
      
      const vals = []
      children.forEach( child => {
        if(vals.indexOf(child[prop])>-1) child.remove()
        vals.push(child[prop])
      })
        
    });
  },
};

window.Neptune = Neptune;

export default Neptune.debug;
