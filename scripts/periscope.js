import Neptune from'./neptune';
import _n from './_n';
//import fetch from '../../node_modules/unfetch'

Neptune.periscope = {
  cache:{}
}

Neptune.periscope.init = ( p ) => { 
   
  p = p || document

  _n.qsa('[neptune-periscope]',p).forEach( ( el ) => {

    Neptune.debug.info( 'periscope:init', {el:el} )

    if(el.getAttribute('neptune-periscope').indexOf('{')<0) 
      return Neptune.periscope.view(el,el.getAttribute('neptune-periscope'));

    let periscope = USON.parse(el.getAttribute('neptune-periscope'))

    if(!!periscope)
      return Neptune.periscope.load(el,periscope[0])
    
  });

};

Neptune.periscope.preload = url => {
  if(!!Neptune.periscope.cache[url]) return false;
  fetch(url)
  .then( (r) => {
    return r.text()
  })
  .then( (html) => {
    Neptune.periscope.cache[url] = html
  })
}

Neptune.periscope.load = (el, view) => {

  return new Promise( (res, rej)=> {

    view.position = view.position || 'contents'

    // if(view.position == 'contents' && !!!view.skeleton )
    //   el.innerHTML = ''

    fetch(view.url,view)
    .then( (r) => {
      if(!!view.path){
        return r.json()  
      }
      return r.text()
    })
    .then( (html) => {

      Neptune.preserved = {}

      _n.qsa('[neptune-permanent]', el).forEach( p => {
        p.setAttribute('permanent',p.getAttribute('neptune-permanent'))
        Neptune.preserved[p.getAttribute('neptune-permanent')] = p
      })

      if(!!view.path){
        html = view.path.split('.').reduce((o,i)=> o[i], html);
      }

      if (!!view.qs) {
        let doc = new DOMParser().parseFromString(html,'text/html')
        html = _n.qs(view.qs,doc).innerHTML;
      }
      
      if(view.position == 'contents'){
        el.innerHTML = html
      }
      if(view.position == 'prepend'){
        el.innerHTML = html + el.innerHTML
      }
      if(view.position == 'append'){
        el.innerHTML = el.innerHTML + html
      }

      _n.trigger(document,'periscope:loaded',false,true,{el:el,view:view})
      _n.qsa('script', el).forEach( (script)=>{
        if(script.getAttribute('type')!='application/javascript') return false;
        if(!!script.textContent && script.textContent.indexOf('(')) eval(script.textContent)
      })
      Neptune.periscope.loaded(el)

      res();
      
    });

  })

}

Neptune.periscope.loaded = (el, callback, clearPermanent, url) => {

  if(!clearPermanent){
    _n.qsa('[neptune-permanent]', el).forEach( p => {
      try{
        p.parentNode.replaceChild(Neptune.preserved[p.getAttribute('neptune-permanent')], p);
      } catch (err) {}
    })
  }

  _n.qsa('script', el).forEach( (script)=>{
    if(script.getAttribute('data-product-json')) return false;
    if(script.hasAttribute('data-json')) return false;
    if(script.hasAttribute('neptune-permanent')) return false;
    if(script.getAttribute('type') && script.getAttribute('type').includes('template')) return false;
    if(script.getAttribute('type') && script.getAttribute('type').includes('json')) return false;
    if(!!script.innerText && script.innerText.indexOf('(')) {
      try { 
        eval(script.innerText)
      } catch(err) {
        //nada
      }
    }
  }) 

  el.classList.remove("periscope-loading");
  _n.trigger(document, "periscope:viewed", false, true, {
    el: el,
    url: url
  });

  if(!!callback) setTimeout(callback,10);

}

Neptune.periscope.view = (el, url, meta, callback, clearPermanent) => { 

  _n.trigger(document,'periscope:view',false,true,{el:el,url:url,meta:meta})
  Neptune.debug.info('periscope:view', {el:el,url:url})

  el.classList.add('periscope-loading')

  // el.innerHTML = ''
  if(!clearPermanent){
    Neptune.preserved = {}
    _n.qsa('[neptune-permanent]', el).forEach( p => {
      p.setAttribute('permanent',p.getAttribute('neptune-permanent'))
      Neptune.preserved[p.getAttribute('neptune-permanent')] = p
    })
  }

  if(!!Neptune.periscope.cache[url]){
    el.innerHTML = Neptune.periscope.cache[url]
    Neptune.periscope.loaded(el, callback, clearPermanent)
  } else {

    fetch(url, {
      method: 'GET',
      credentials: 'include',
    })
    .then( (r) => {
      return r.text()
    })
    .then( (html) => {

     _n.trigger(document,'periscope:preview',false,true,{el:el,url:url,meta:meta})
      
      el.innerHTML = html

      Neptune.periscope.loaded(el, callback, clearPermanent, url)

    });
  }

}

window.Neptune = Neptune

export default Neptune.periscope;
