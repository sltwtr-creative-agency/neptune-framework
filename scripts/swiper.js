import Neptune from './neptune'
import Swiper from 'swiper/bundle';

Neptune.swiper = {

  init: (p) => {

    p = p || document
    
    _n.qsa('[data-swiper], [neptune-swiper]',p).forEach( ( el ) => {

      //console.log('swiper init', p, el)

      if(!el.classList.contains('swiper-container-initialized') && !el.classList.contains('swiper-hold') && !el.swiper)
        Neptune.swiper.swiper(el)
        _n.trigger(el, 'Swiper::Initialized')

    })
    // setTimeout(Neptune.swiper.reset,150)

  },

  reset: function() {

      document.querySelectorAll('[data-swiper]').forEach(function(el){

          if(!el.hasOwnProperty('swiper')) return;

          if(el.hasAttribute('data-swiper-reset-widths')){
            el.querySelectorAll('.swiper-slide').forEach(function(slide){
              slide.style.width = slide.firstElementChild.offsetWidth +"px"
              slide.style.opacity = 1
            })
          }
          el.swiper.update()

      })

  },

  swiper: (el) => {
    
    if (!el) return;
    if (!!el.swiper) return;

    if(el.querySelectorAll('.swiper-slide').length < 2) return;

    let config = {
      scrollbar: !!el.querySelector('.swiper-scrollbar')?{
        el: '.swiper-scrollbar',
        hide: true,
      }:false,
      a11y: {
        enabled: true,
      },
      on: {
        init: swiper => {
          const slide = _n.qsa('.swiper-slide', el)[swiper.activeIndex]
          if(!!slide && slide.hasAttribute('onactive')){
            eval( slide.getAttribute('onactive').replace(/this\./g,'slide.') )
          }
          if(el.hasAttribute('onchange')){
            eval( el.getAttribute('onactive').replace(/this\./g,'slide.') )
          }
        },
        activeIndexChange: swiper =>{
          const slide = _n.qsa('.swiper-slide', el)[swiper.activeIndex]
          if(!!slide && slide.hasAttribute('onactive')){
            eval( slide.getAttribute('onactive').replace(/this\./g,'slide.') )
          }
          if(el.hasAttribute('onchange')){
            eval( el.getAttribute('onactive').replace(/this\./g,'slide.') )
          }
        }
      }
    };


    if(!!el.getAttribute('neptune-swiper')){
      config = Object.assign(USON.parse(el.getAttribute('neptune-swiper'))[0],config)
      config = Neptune.swiper.setMethods(config)
      // config = Neptune.swiper.eventListeners(config)
    }

    el.the_swiper = true

    window.the_el = el

    for(var ai in el.attributes){

        if(!ai) continue;

        if(!!el.attributes[ai].nodeName && el.attributes[ai].nodeName.indexOf('data-swiper-')===0){

            var key = el.attributes[ai].nodeName.replace('data-swiper-','')
            var key = key.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); });

            var val = el.attributes[ai].nodeValue

            if(val == 'true') val = true
            if(val == 'false') val = false

            if(typeof val != 'boolean'){

                try { 
                    if(!isNaN(val)) val = parseFloat(val) 
                }
                catch(e) {
                // console.log(e)
                }
                try {
                    if(typeof JSON.parse(val) == 'object') val = JSON.parse(val)
                }
                catch(e) {
                    // console.log(e)
                }

            }

            config[key] = val

        }
    }

    el.swiper = new Swiper(el,config)

    if (el.querySelectorAll('.swiper-next').length) {
        el.querySelector('.swiper-next').addEventListener('click', function(){
        el.swiper.slideNext()
        })
    }
    if (el.querySelectorAll('.swiper-prev').length) {
        el.querySelector('.swiper-prev').addEventListener('click', function(){
        el.swiper.slidePrev()
        })
    }

    el.swiper.on('slideChange', swiper=>{
      _n.trigger(el,'slideChange', true, true, swiper)
    })

    el.swiper.on('slideChangeTransitionEnd', function(swiper) { 

      if(!!el.getAttribute('data-onchange')){
          window[el.getAttribute('data-onchange')](swiper)
      }

    });

  },

  setMethods: (object) => {
    // Set a method within a string in JSON by prepending it with "method:"
    for (const [key, value] of Object.entries(object)) {
      switch (typeof value) {
        case 'object': {
          Neptune.swiper.setMethods(value);
          break;
        }
        case 'string': {
          if (value.includes('method:')) {
            const parameters = value.substring(
              value.indexOf("(") + 1,
              value.indexOf(")")
            ); 
            const statement = value.substring(
              value.indexOf("{") + 1,
              value.lastIndexOf("}")
            );
            object[key] = new Function(parameters, statement)
          }
          break;
        }
      }
    }

    return object
  },

  eventListeners: (object) => {
    
    if(!!object.on) { 

      for (const [key, value] of Object.entries(object.on)) {
        
        object.on[key] = function(){eval(value)}
      
      }

    }

    return object
  }
}
window.Neptune = Neptune

export default Neptune.swiper;
