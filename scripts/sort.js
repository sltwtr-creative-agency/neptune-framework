import Neptune from "./neptune";
import _n from "./_n";

Neptune.sort = {
  init: function(p) {
    p = p || document;

    Neptune.debug.info("sort:init", { el: p });

    _n.qsa("[neptune-sort]", p).forEach((el) => {
      let _sort = {};

      try {
        if (!!el.getAttribute("neptune-sort")) {
          _sort = USON.parse(
            el
              .getAttribute("neptune-sort")
              .replace(/'/g, '"')
              .replace(/`/g, "'")
              .replace(/</g, "[")
              .replace(/>/g, "]"),
            "array"
          )[0];
        }
      } catch (error) {}

      _n.sort(el, _sort.on || "textContent.trim()", _sort.as, _sort.reverse || false, _sort.order || false);
    });
  },
};

window.Neptune = Neptune;

export default Neptune.debug;
